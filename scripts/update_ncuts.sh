#!/bin/sh
#
# $1 is the analysis name

# SUFFIX="scripts"
# HEP_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
# HEP_DIR=${HEP_DIR%$SUFFIX}


export HEP_DIR=$(pwd)
export HEP_DIR=$HEP_DIR"/../../"

export FILE=$HEP_DIR/Analysis/$1/src/$1.cxx
export CUTS=$(awk '/unsigned *number_of_cuts *= */{print $0}' $FILE | sed -e 's/[ \t_A-Za-z=;]//g')


export unamestr=$(uname)

if [[ "$unamestr" == 'Darwin' ]]; then
	sed -i ".original" "s/NUMBER_OF_CUTS=.*/NUMBER_OF_CUTS=$CUTS/g" $HEP_DIR/Analysis/$1/app_Makefile
	rm *.original
else
	sed -i "/NUMBER_OF_CUTS=/c\NUMBER_OF_CUTS=$CUTS" $HEP_DIR/Analysis/$1/app_Makefile
fi

make clean -f app_Makefile
make -f app_Makefile