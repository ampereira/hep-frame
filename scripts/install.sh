#!/bin/bash
#
# Script that installs the HEP-Frame tools
# 
# $1 Specific boost lib dir, if any

export SUFFIX="scripts"
export HEP_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export HEP_DIR=${HEP_DIR%$SUFFIX}

source realpath.sh

if [ "$#" -eq 1 ]
  then
    # BOOST_DIR=$(readlink -f $1)
    export BOOST_DIR=$(realpath $1)
  else
	echo ""
	echo "---------------------------------------------------"
	echo "HEP-Frame :: Downloading and Compiling the Boost Library"
	echo "---------------------------------------------------"
	echo ""

    cd ${HEP_DIR}/tools/boost-1_62_0

  	curl --insecure -O -L http://downloads.sourceforge.net/project/boost/boost/1.62.0/boost_1_62_0.tar.gz

  	echo "Unpacking Boost"
    tar -xzvf boost_1_62_0.tar.gz
    rm boost_1_62_0.tar.gz
    
    cd boost_1_62_0

    echo "Installing boost"
    ./bootstrap.sh
    ./b2 install --prefix=${BOOST_DIR}

    cd ..
    rm -rf boost_1_62_0

    cd ${HEP_DIR}/scripts
fi

echo "#!/bin/bash" > ${HEP_DIR}/scripts/setEnv.sh
echo "export LD_LIBRARY_PATH=${BOOST_DIR}/lib:$LD_LIBRARY_PATH" >> ${HEP_DIR}/scripts/setEnv.sh

mv generic_Makefile generic_Makefile_temp
line=$(head -n 1 generic_Makefile_temp)
if [[ "$line" == BOOST* ]]; then
echo "$(tail -n +2 generic_Makefile_temp)" > generic_Makefile_temp
fi
echo "BOOST_DIR=${BOOST_DIR}" | cat - generic_Makefile_temp > generic_Makefile
rm generic_Makefile_temp

mv generic_Makefile_delphes generic_Makefile_delphes_temp
line=$(head -n 1 generic_Makefile_delphes_temp)
if [[ "$line" == BOOST* ]]; then
echo "$(tail -n +2 generic_Makefile_delphes_temp)" > generic_Makefile_delphes_temp
fi
echo "BOOST_DIR=${BOOST_DIR}" | cat - generic_Makefile_delphes_temp > generic_Makefile_delphes
rm generic_Makefile_delphes_temp

mv ../lib/Makefile ../lib/Makefile_temp
line=$(head -n 1 ../lib/Makefile_temp)
if [[ "$line" == BOOST* ]]; then
echo "$(tail -n +2 ../lib/Makefile_temp)" > ../lib/Makefile_temp
fi
echo "BOOST_DIR=${BOOST_DIR}" | cat - ../lib/Makefile_temp > ../lib/Makefile
rm ../lib/Makefile_temp

echo ""
echo "---------------------------------------------------"
echo "HEP-Frame :: Compiling the Analysis Generator tool"
echo "---------------------------------------------------"
echo ""

# compiles analysis generator
cd ${HEP_DIR}/tools/analysis_generator
mv Makefile Makefile_temp
line=$(head -n 1 Makefile_temp)
if [[ "$line" == BOOST* ]]; then
echo "$(tail -n +2 Makefile_temp)" > Makefile_temp
fi
echo "BOOST_DIR=${BOOST_DIR}" | cat - Makefile_temp > Makefile
rm Makefile_temp

make clean
make

echo ""
echo "---------------------------------------------------"
echo "HEP-Frame :: Compiling the Class Generator tool"
echo "---------------------------------------------------"
echo ""

# compiles class generator
cd $HEP_DIR/tools/class_generator
mv Makefile Makefile_temp
line=$(head -n 1 Makefile_temp)
if [[ "$line" == BOOST* ]]; then
echo "$(tail -n +2 Makefile_temp)" > Makefile_temp
fi
echo "BOOST_DIR=${BOOST_DIR}" | cat - Makefile_temp > Makefile
rm Makefile_temp

make clean
make

echo ""
echo "---------------------------------------------------"
echo "HEP-Frame :: Compiling the Interface Generator tool"
echo "---------------------------------------------------"
echo ""

# compiles the interface generator
cd $HEP_DIR/tools/interface_generator

make clean
make

echo ""
echo "---------------------------------------------------"
echo "HEP-Frame :: Compiling the Record Parser tool"
echo "---------------------------------------------------"
echo ""

# compiles record parser
cd $HEP_DIR/tools/record_parser
mv Makefile Makefile_temp
line=$(head -n 1 Makefile_temp)
if [[ "$line" == BOOST* ]]; then
echo "$(tail -n +2 Makefile_temp)" > Makefile_temp
fi
echo "BOOST_DIR=${BOOST_DIR}" | cat - Makefile_temp > Makefile
rm Makefile_temp

make clean
make


cd $HEP_DIR/scripts/
./copyLibsOSX.sh $BOOST_DIR



# echo ""
# echo "---------------------------------------------------"
# echo "HEP-Frame :: Compiling ExRootAnalysis"
# echo "---------------------------------------------------"
# echo ""

# # compiles record parser
# cd $HEP_DIR/tools/ExRootAnalysis

# make clean
# make

# echo ""
# echo "---------------------------------------------------"
# echo "HEP-Frame :: Compiling Delphes"
# echo "---------------------------------------------------"
# echo ""

# # compiles record parser
# cd $HEP_DIR/tools/Delphes-3.3.2

# make clean
# make
