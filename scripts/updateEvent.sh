#!/bin/bash

# SUFFIX="scripts"
# HEP_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
# HEP_DIR=${HEP_DIR%$SUFFIX}

export HEP_DIR=$(pwd)
export HEP_DIR=$HEP_DIR"/../../"

cd $HEP_DIR/lib

export STRING="#include \"$HEP_DIR/Analysis/$1/src/$1_Event.h\""

export STRING2=$(sed '4q;d' $HEP_DIR/lib/src/Event.h)

if [ "$STRING" != "$STRING2" ]; then

	echo "#ifndef EVENT_h" > $HEP_DIR/lib/src/Event.h
	echo "#define EVENT_h" >> $HEP_DIR/lib/src/Event.h
	echo "" >> $HEP_DIR/lib/src/Event.h
	echo "#include \"$HEP_DIR/Analysis/$1/src/$1_Event.h\"" >> $HEP_DIR/lib/src/Event.h
	echo "" >> $HEP_DIR/lib/src/Event.h
	echo "#endif" >> $HEP_DIR/lib/src/Event.h

	../scripts/makeLib.sh

elif [ ! -e $HEP_DIR/lib/lib/libHEPFrame.a ]; then

	echo "#ifndef EVENT_h" > $HEP_DIR/lib/src/Event.h
	echo "#define EVENT_h" >> $HEP_DIR/lib/src/Event.h
	echo "" >> $HEP_DIR/lib/src/Event.h
	echo "#include \"$HEP_DIR/Analysis/$1/src/$1_Event.h\"" >> $HEP_DIR/lib/src/Event.h
	echo "" >> $HEP_DIR/lib/src/Event.h
	echo "#endif" >> $HEP_DIR/lib/src/Event.h

	../scripts/makeLib.sh
fi
