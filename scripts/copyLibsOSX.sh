#!/bin/bash
#
# Script that installs the BOOST libs for OSX
# 
# $1 Specific boost lib dir

export SUFFIX="scripts"
export HEP_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export HEP_DIR=${HEP_DIR%$SUFFIX}

source realpath.sh

export BOOST_DIR=$(realpath $1)

export unamestr=$(uname)

if [[ "$unamestr" == 'Darwin' ]]; then
	mkdir osx_libs

	cp $BOOST_DIR/lib/libboost_program_options.dylib osx_libs/
	cp $BOOST_DIR/lib/libboost_system.dylib osx_libs/
	cp $BOOST_DIR/lib/libboost_thread.dylib osx_libs/

	cp $BOOST_DIR/lib/libboost_program_options.dylib ../tools/analysis_generator/bin/
	cp $BOOST_DIR/lib/libboost_program_options.dylib ../tools/class_generator/bin/
	cp $BOOST_DIR/lib/libboost_program_options.dylib ../tools/interface_generator/bin/
	cp $BOOST_DIR/lib/libboost_program_options.dylib ../tools/record_parser/bin/
fi