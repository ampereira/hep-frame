#!/bin/sh

# $1 Analysis Name

# echo $(pwd)

# SUFFIX="scripts"
# HEP_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# echo $HEP_DIR
# echo $( dirname "${BASH_SOURCE[0]}" )

export CLASS_NAME="$1_Event"
# HEP_DIR=${HEP_DIR%$SUFFIX}

export HEP_DIR=$(pwd)
export HEP_DIR=$HEP_DIR"/../../"
# echo $CENAS

# creates the event data interface
cd $HEP_DIR/tools/interface_generator/bin
./EDGen $HEP_DIR/Analysis/$1/src/ $CLASS_NAME.h