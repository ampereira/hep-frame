#!/bin/bash
#
# Script that updates HEP-Frame
# 
# $1 Specific boost lib dir, if any

# if [ "$#" -eq 1 ]
#   then
#   	BOOST_DIR=$1
#   else
#   	echo "Assuming that Boost is installed on system root"
# fi


export SUFFIX="scripts"
export HEP_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export HEP_DIR=${HEP_DIR%$SUFFIX}


export line=$(head -n 1 $HEP_DIR/lib/Makefile)
export BOOST_DIR=${line:10}

# Downloads the new version of the framework
curl -o hep-frame-update.zip 'https://bitbucket.org/ampereira/hep-frame/get/master.zip'
unzip hep-frame-update.zip
mv ampereira-hep-frame-* hep-frame-update

# Replace the old files
mv $HEP_DIR/lib/ $HEP_DIR/lib_old/
mv $HEP_DIR/tools/ $HEP_DIR/tools_old/

mv hep-frame-update/lib $HEP_DIR/
mv hep-frame-update/tools $HEP_DIR/

cd $HEP_DIR

mv scripts scripts_old
mv scripts_old/hep-frame-update/scripts/ $HEP_DIR/

cd $HEP_DIR/scripts/

source install.sh $BOOST_DIR

rm -rf ../scripts_old/
rm -rf ../lib_old/
rm -rf ../tools_old/

echo ""
echo ""
echo "----------------------------------------"

echo "HEP-Frame :: Updated successfully"

echo "----------------------------------------"
echo ""
echo ""

