#!/bin/bash
#
# Script that updates HEP-Frame
# 
# $1 Specific boost lib dir, if any

# if [ "$#" -eq 1 ]
#   then
#   	BOOST_DIR=$1
#   else
#   	echo "Assuming that Boost is installed on system root"
# fi


export SUFFIX="scripts"
export HEP_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export HEP_DIR=${HEP_DIR%$SUFFIX}


export SUFFIX2="BOOST_DIR="
export line=$(head -n 1 $HEP_DIR/lib/Makefile)
export BOOST_DIR=${line%$SUFFIX2}

# Downloads the new version of the framework
curl -o hep-frame-update.zip 'https://bitbucket.org/ampereira/hep-frame/get/master.zip'
unzip hep-frame-update.zip
mv ampereira-hep-frame-* hep-frame-update

# Replace the old files
rm -rf $HEP_DIR/lib
rm -rf $HEP_DIR/tools

mv hep-frame-update/lib $HEP_DIR/
mv hep-frame-update/tools $HEP_DIR/

cd $HEP_DIR

mv scripts scripts-old
mv scripts-old/hep-frame-update/scripts $HEP_DIR

rm -rf scripts-old

cd $HEP_DIR/scripts

source install.sh $BOOST_DIR

echo ""
echo ""
echo "----------------------------------------"

echo "HEP-Frame :: Updated successfully"

echo "----------------------------------------"
echo ""
echo ""

