#!/bin/bash

# Script to create a new analysis
# Beta version!

# $1 Analysis name
# $2 Input root file - only to create the data structure
# $3 Set "delphes" or a specific tree name inside the root tuple

export SUFFIX="scripts"
export HEP_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export HEP_DIR=${HEP_DIR%$SUFFIX}

source realpath.sh

if [ "$#" -lt 2 ]
  then
    echo "Not enough arguments supplied"
    echo "1st: Analysis name"
    echo "2nd: Input root file - only to create the data structure"
    echo "3rd: Set \"delphes\" or a specific tree name inside the root tuple"

    exit 0
fi

mkdir -p $HEP_DIR/Analysis/$1/src


export CLASS_NAME="$1_Event"
# ROOT_TUPLE=$(readlink -f $2)
export ROOT_TUPLE=$(realpath $2)
echo $ROOT_TUPLE

cp $HEP_DIR/scripts/Makefile $HEP_DIR/Analysis/$1/Makefile
echo -e "\n\t@../../scripts/update_ncuts.sh $1" >> $HEP_DIR/Analysis/$1/Makefile

# creates the event class file
cd $HEP_DIR/tools/class_generator/bin

case "$#" in
  # no delphes and no specific tree
  2) 
    # copy the Makefile
    echo "CLASS=$1" | cat - $HEP_DIR/scripts/generic_Makefile > $HEP_DIR/Analysis/$1/app_Makefile
    ./classGen -f $ROOT_TUPLE -c $CLASS_NAME -d $HEP_DIR/Analysis/$1/src/
      #rm $HEP_DIR"/tools/class_generator/bin/"$1"_Event.h"

    ;;
  # delphes or specific tree
  3) if [ "$3" = "delphes" ]
      then
      # copy the Makefile
      echo "CLASS=$1" | cat - $HEP_DIR/scripts/generic_Makefile_delphes > $HEP_DIR/Analysis/$1/app_Makefile
        ./classGen -f $ROOT_TUPLE -c $CLASS_NAME -d $HEP_DIR/Analysis/$1/src/ -t true
        mv $HEP_DIR"/tools/class_generator/bin/"$1"_Event.h" $HEP_DIR/Analysis/$1/src/
        mv $HEP_DIR"/tools/class_generator/bin/"$1"_Event.cxx" $HEP_DIR/Analysis/$1/src/
      else
      # copy the Makefile
      echo "CLASS=$1" | cat - $HEP_DIR/scripts/generic_Makefile > $HEP_DIR/Analysis/$1/app_Makefile
        ./classGen -f $ROOT_TUPLE -c $CLASS_NAME -d $HEP_DIR/Analysis/$1/src/ -a $3
        rm $HEP_DIR"/tools/class_generator/bin/"$1"_Event.h"
    fi
    ;;
esac


# creates the analysis template
cd $HEP_DIR/tools/analysis_generator/bin

if [ "$3" != "delphes"  ] && [ "$#" = 3 ]
  then
    ./analysisGenerator -d $HEP_DIR/Analysis/$1/src/ -a $1 -t $3
    # creates the event data interface
    cd $HEP_DIR/tools/interface_generator/bin
    ./EDGen $HEP_DIR/Analysis/$1/src/ $CLASS_NAME.h
  else
    ./analysisGenerator -d $HEP_DIR/Analysis/$1/src/ -a $1
    # creates the event data interface
    cp $HEP_DIR/scripts/EventInterfaceDelphes.h $HEP_DIR/Analysis/$1/src/EventInterface.h
fi

export unamestr=$(uname)

if [[ "$unamestr" == 'Darwin' ]]; then
  mkdir $HEP_DIR/Analysis/$1/bin

  cp $HEP_DIR/scripts/osx_libs/* $HEP_DIR/Analysis/$1/bin/
fi


# Update the Event header on HEP-Frame
echo "#ifndef EVENT_h" > $HEP_DIR/lib/src/Event.h
echo "#define EVENT_h" >> $HEP_DIR/lib/src/Event.h
echo "" >> $HEP_DIR/lib/src/Event.h
echo "#include \"$HEP_DIR/Analysis/$1/src/$1_Event.h\"" >> $HEP_DIR/lib/src/Event.h
echo "" >> $HEP_DIR/lib/src/Event.h
echo "#endif" >> $HEP_DIR/lib/src/Event.h
