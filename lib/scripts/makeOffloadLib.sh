BOOST_DIR=/home/ampereira/tools/boost/1.60.0

# Remove old files
ORIGINAL_FOLDER="$(pwd)"

cd ../Analysis/TTHoffload/offload/mic
rm -f build/*
rm -f lib/*

mkdir -p build/
mkdir -p lib/

# Compile new lib
icpc -fopenmp -c -Wall -std=c++11 src/TTH_offload.cxx -o build/TTH_offload.o
ar -r lib/libOffload.a build/TTH_offload.o

# Build GPU offload
cd ../gpu

rm -f build/*
rm -f lib/*

mkdir -p build/
mkdir -p lib/
