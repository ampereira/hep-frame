#include "TaskStealingScheduler.h"

extern boost::mutex stop_time;

Scheduler::Scheduler (unsigned n_propositions) {
	number_of_props  	 = n_propositions;
	current_data		 = 0;
	current_prop		 = 0;
	final_evs			 = 0;
	max_prop_time		 = 0.0;

	prop_weight			 = new float[n_propositions];
	prop_exec_time 	 	 = new double[n_propositions];
	prop_passed		 	 = new long unsigned[n_propositions];
	prop_failed		 	 = new long unsigned[n_propositions];
	props_start_deps	 = new unsigned[n_propositions];
	//props_end_deps		 = new unsigned[n_propositions];
	props_queue			 = new unsigned[n_propositions];
	props_deps 		 	 = new unsigned*[n_propositions];
	prop_up_neighbours	 = new vector<unsigned>[number_of_props];
	prop_down_neighbours = new vector<unsigned>[number_of_props];

	for (unsigned i = 0; i < n_propositions; i++) {
		props_start_deps[i] = 0;
		props_queue[i] = 0;
		//props_end_deps[i] = 0;
		prop_passed[i] = 0;
		prop_failed[i] = 0;

		props_deps[i] = new unsigned[n_propositions];

		for (unsigned j = 0; j < n_propositions; j++)
			props_deps[i][j] = 0;
	}
}

Scheduler::~Scheduler (void) {
	/*delete prop_exec_time;
	delete prop_passed;
	delete prop_failed;
	delete props_start_deps;
	delete props_end_deps;
	delete current_prop_pos;
	delete current_prop_pos_completed;
	delete[] prop_up_neighbours;
	delete[] prop_down_neighbours;
	delete[] props_deps;*/
}

void Scheduler::report (void) {
	int passed = -1;
	for (unsigned i = 0; i < number_of_props; i++) {
		cout << "\tCut #" << i+1 << ": " << prop_passed[i] << endl;

		if (passed == -1 || passed > prop_passed[i])
			passed = prop_passed[i];
	}

	cout << endl << "\t" << passed << " events passed all cuts" << endl;
}

void Scheduler::reportDebug (void) {
	int passed = -1;
	cout << endl << "\tCut #X: passed\tfailed\tfilter\tweight" << endl;
	cout << "\t-----------------------------------------------" << endl;
	for (unsigned i = 0; i < number_of_props; i++) {
		cout << "\tCut #" << i << ": " << prop_passed[i] << "\t" << prop_failed[i] << "\t"<<std::setprecision(5) << ((float)prop_passed[i]/(float)(prop_passed[i]+prop_failed[i])) << "\t" <<std::setprecision(6) << prop_weight[i] << endl;

		if (passed == -1 || passed > prop_passed[i])
			passed = prop_passed[i];
	}

	cout << endl << "\t" << passed << " events passed all cuts" << endl;
	


	cout << number_of_elements << endl;
	long unsigned stuff =0;
	for (int i = 0; i < number_of_elements; ++i)
	{
		if (current_prop_pos_completed[i] == number_of_props)
			stuff ++;
	}
	cout << "sht " << stuff << endl;
}

void Scheduler::reportTable (void) {
	cout << endl;
	cout << "Proposition Table" << endl;

	for (auto v : props_table) {
		for (auto e : v)
			cout << e << " ";
		cout << endl << "------------------" << endl;
	}

	cout << endl << endl;
}

void Scheduler::passProposition (unsigned prop_id, unsigned data_element_id) {
	pass_prop.lock();
	// evs_props[data_element_id][prop_id] = 1;
	prop_passed[prop_id]++;

	pass_all_props.lock();
	current_prop_pos_completed[data_element_id]++;
	pass_all_props.unlock();

	if (prop_id == number_of_props - 1) {
		current_prop_pos[data_element_id] = -1;
		final_evs++;
	}
	pass_prop.unlock();
}

void Scheduler::failPropositionOld (unsigned prop_id) {
		current_prop = 0;
		prop_failed[prop_id]++;
}

void Scheduler::failProposition (unsigned prop_id, unsigned data_element_id) {
	pass_prop.lock();
	// evs_props[data_element_id][prop_id] = 2;

	if (current_prop_pos[data_element_id] != -1) {
		current_prop_pos[data_element_id] = -1;
		prop_failed[prop_id]++;
	}
	pass_prop.unlock();
}

chrono::time_point<chrono::high_resolution_clock> Scheduler::propositionStart () {
	return chrono::high_resolution_clock::now();
}

void Scheduler::propositionStop (chrono::time_point<chrono::high_resolution_clock> init, unsigned prop_id) {
	auto end_time = chrono::high_resolution_clock::now();

	chrono::duration<double> elapsed = end_time - init;

	double elps = elapsed.count() * 10000000;	// to nanosseconds

	stop_time.lock();

	if(prop_exec_time[prop_id] != 0)
		prop_exec_time[prop_id] = (prop_exec_time[prop_id] + elps)/2;
	else
		prop_exec_time[prop_id] = elps;

	if (prop_exec_time[prop_id] > max_prop_time)
		max_prop_time = prop_exec_time[prop_id];

	stop_time.unlock();
}

void Scheduler::setPropositionDep (unsigned prop1, unsigned prop2) {
	props_start_deps[prop1 - 1]++;
	//props_end_deps[prop2 - 1]++;

	props_deps[prop1 - 1][prop2 - 1] = 1;
}

// optimizar
vector< vector<unsigned> > Scheduler::createListDeps (void) {
	vector< vector<unsigned> > list_of_deps;
	unsigned prop;

	for (unsigned i = 0; i < number_of_props; i++) {
		// current prop starts a dependency
		if (props_start_deps[i] != 0) {
			vector< vector<unsigned> > other_paths;	// stores other possible directions from path x
			vector<unsigned> chain_of_deps;
			unsigned next_prop;

			chain_of_deps.push_back(i);
			other_paths.push_back(chain_of_deps);

			// follow the remaining paths
			while (!other_paths.empty()) {
				vector<unsigned> current = other_paths.back();
				other_paths.pop_back();
				bool end_of_deps = false;

				while (!end_of_deps) {
					bool first = false;

					prop = current.back();

					for (unsigned j = 0; j < number_of_props; j++) {
						if (props_deps[prop][j] != 0) {
							if (!first) {
								next_prop = j;
								current.push_back(next_prop);
								first = true;
							} else {
								// TODO: optimizar isto
								vector< unsigned> aux = current;
								aux.pop_back();
								aux.push_back(j);
								other_paths.push_back(aux);
							}
						}

						if (j == number_of_props - 1){
							if (!first)
								end_of_deps = true;
							else
								prop = next_prop;
						}
					}
				}

				for (unsigned j = 0; j < current.size(); j++) {
					vector<unsigned> up_neigh, down_neigh;

					for (unsigned k = 0; k < current.size(); k++)
						if (k > j)
							down_neigh.push_back(current[k]);
						else
							up_neigh.push_back(current[k]);

					addUpNeighbours(current[j], up_neigh);
					addDownNeighbours(current[j], down_neigh);
				}

				list_of_deps.push_back(current);
			}
		}
	}

	return list_of_deps;
}

void Scheduler::createTable (vector< vector<unsigned> > list_of_deps) {
	unsigned props_inserted[number_of_props];

	memset(props_inserted, 0, number_of_props*sizeof(unsigned));

	// adds dependent props
	for (auto v : list_of_deps) {
		if (props_table.size() < v.size())
			props_table.resize(v.size());

		for (unsigned i = 0; i < v.size(); i++) {
			props_table[i].push_back(v[i]);

			if (props_queue[v[i]] < i)
				props_queue[v[i]] = i;
		}
	}
	// avoids duplicates in the same queue
	for (unsigned k = 0; k < number_of_props; k++)
		for (int i = props_table.size()-1; i >= 0; i--) {
			for (int j = 0; j < props_table[i].size(); j++) {
				if (k == props_table[i][j]) {
					if (props_inserted[k] > 0) {
						props_table[i].erase(props_table[i].begin() + j);
						j--;
					} else
						props_inserted[props_table[i][j]]++;
				}
			}
		}

	// checks if there are not any dependences
	if (props_table.empty())
		props_table.resize(1);

	// adds non dependent props
	for (unsigned i = 0; i < number_of_props; ++i)
		if (props_inserted[i] == 0) {
			props_table[0].push_back(i);
			props_queue[i] = 0;
		}
}

bool Scheduler::shiftUp (unsigned prop) {
	// checks if it has no neighbours
	if (prop_up_neighbours[prop].empty()) {
		// check if it has a queue to go
		int queue = props_queue[prop];

		if (queue - 1 >= 0) {
			for (unsigned i = 0; i < props_table[queue].size(); i++)
				if (props_table[queue][i] == prop)
					props_table[queue].erase(props_table[queue].begin() + i);

			props_table[queue-1].push_back(prop);
			props_queue[prop]--;
		} else
			return false;
	} else {
		// if it has neighbours
		int queue = props_queue[prop];

		// check if it has a queue to go
		if ((queue - (int) prop_up_neighbours[prop].size() - 1) >= 0 ) {
			// updates the queue of each prop
			for (unsigned i = 0; i < props_table[queue].size(); i++)
				if (props_table[queue][i] == prop)
					props_table[queue].erase(props_table[queue].begin() + i);

			props_table[queue-1].push_back(prop);
			props_queue[prop]--;

			for (auto e : prop_up_neighbours[prop]) {
				if (props_queue[e] > 0) {
					for (unsigned i = 0; i < props_table[props_queue[e]].size(); i++)
						if (props_table[props_queue[e]][i] == e)
							props_table[props_queue[e]].erase(props_table[props_queue[e]].begin() + i);
					
					props_table[props_queue[e]-1].push_back(e);
					props_queue[e]--;
				}
			}
		} else
			return false;
	}

	return true;
}

bool Scheduler::shiftDown (unsigned prop) {
	// checks if it has no neighbours
	if (!(props_table[props_queue[prop]].size() == 1 && props_queue[prop] == props_table.size()-1)) {
		if (props_table[props_queue[prop]].size() != 1) {
			if (prop_down_neighbours[prop].empty()) {
				// check if it has a queue to go
				int queue = props_queue[prop];

				if (queue < (int) props_table.size() - 1) {
					for (unsigned i = 0; i < props_table[queue].size(); i++)
						if (props_table[queue][i] == prop)
							props_table[queue].erase(props_table[queue].begin() + i);

					props_table[queue+1].push_back(prop);
					props_queue[prop]++;
				} else {
					for (unsigned i = 0; i < props_table[queue].size(); i++)
						if (props_table[queue][i] == prop)
							props_table[queue].erase(props_table[queue].begin() + i);

					// increases the table size
					vector<unsigned> v;
					v.push_back(prop);
					props_table.push_back(v);
					props_queue[prop]++;
				}
			} else {
				// if it has neighbours
				int queue = props_queue[prop];

				// check if it has a queue to go
				if ((queue + prop_down_neighbours[prop].size() + 1) < props_table.size()) {
					// updates the queue of each prop
					for (unsigned i = 0; i < props_table[queue].size(); i++)
						if (props_table[queue][i] == prop)
							props_table[queue].erase(props_table[queue].begin() + i);

					props_table[queue+1].push_back(prop);
					props_queue[prop]++;

					for (auto e : prop_down_neighbours[prop]) {
						// checks if the neighbour is still after the prop
						if (props_queue[e] <= props_queue[prop]) {
							for (unsigned i = 0; i < props_table[props_queue[e]].size(); i++)
								if (props_table[props_queue[e]][i] == e)
									props_table[props_queue[e]].erase(props_table[props_queue[e]].begin() + i);	// erases from old position


			//				cout << props_table.size() << "\t" << props_queue[e] +1 << "\t"<< e+1 << endl;
							if (props_table.size() > props_queue[e] +1)
								props_table[props_queue[e]+1].push_back(e);	// adds to new position
							else{
								vector<unsigned> vv;
								vv.push_back(e);
								props_table.push_back(vv);
							}
							props_queue[e]++;
						}
					}
				} else {
					// updates the queue of each prop
					for (unsigned i = 0; i < props_table[queue].size(); i++)
						if (props_table[queue][i] == prop)
							props_table[queue].erase(props_table[queue].begin() + i);

					props_table[queue+1].push_back(prop);
					props_queue[prop]++;


					for (auto e : prop_down_neighbours[prop]) {
						for (unsigned i = 0; i < props_table[props_queue[e]].size(); i++)
							if (props_table[props_queue[e]][i] == e)
								props_table[props_queue[e]].erase(props_table[props_queue[e]].begin() + i);
						if (props_queue[e]+1 < props_table.size()) {
							props_table[props_queue[e]+1].push_back(e);
						} else {
							// increases the table size
							vector<unsigned> v;
							v.push_back(e);
							props_table.push_back(v);
						}
						props_queue[e]++;
					}
				}
			}
		} else {
			return false;
		}
		return true;
	} else{
		return false;
	}
}

// deprecated
void Scheduler::addUpNeighbour (unsigned prop, unsigned neighbour) {
	if (prop != neighbour)
		prop_up_neighbours[prop].push_back(neighbour);
}

// deprecated
void Scheduler::addDownNeighbour (unsigned prop, unsigned neighbour) {
	if (prop != neighbour)
		prop_down_neighbours[prop].push_back(neighbour);
}

void Scheduler::addUpNeighbours (unsigned prop, vector<unsigned> neighbours) {
	for (auto e1 : neighbours){
		bool insert = true;

		for (auto e2 : prop_up_neighbours[prop]) {
			if (e1 == e2)
				insert = false;
		}

		if (insert && e1 != prop)
			prop_up_neighbours[prop].push_back(e1);
	}
}

// assumes that prop is in the neighbours vector
void Scheduler::addDownNeighbours (unsigned prop, vector<unsigned> neighbours) {
	for (auto e1 : neighbours){
		bool insert = true;

		for (auto e2 : prop_down_neighbours[prop]) {
			if (e1 == e2)
				insert = false;
		}

		if (insert && e1 != prop)
			prop_down_neighbours[prop].push_back(e1);
	}
}

// ACESSOS A TABLE PODEM ESTAR INEFECIENTES!
unsigned Scheduler::getPropositionOld (unsigned data_element_id) {
	unsigned val;

	/*#pragma omp critical
	{	
	if (data_element_id == current_data) {
		int prop = current_prop;	// current prop position
		cout << "thread " << omp_get_thread_num() << " ev " << data_element_id << " current_data " << current_data << endl;
		cout << "current_prop " << prop << endl;

		// checks if it is after the final prop
		if (prop < (int) number_of_props) {
			for (auto a : props_table) {
				if (prop < (int) a.size()) {
					for (auto e : a) {
						if (prop == 0) {
							current_prop++;	// updates to the next prop to execute
							val = e;
						}
						prop--;
					}
				} else {
					prop -= a.size();
				}
			}
		} else {
			current_prop = 1;	// updates to the next prop to execute

			val = props_table[0][0];
		}
		cout << "thread " << omp_get_thread_num() << " ev " << data_element_id << " prop " << val << endl << endl;
	} else {
		// new data element!
		current_data = data_element_id;
		current_prop = 1;	// updates to the next prop to execute

		val = props_table[0][0];
	}

	}

	return val;*/
	return NO_PROPOSITION;
}

unsigned Scheduler::getProposition (unsigned data_element_id) {
	unsigned prop = NO_PROPOSITION;
	
	int _pos_ = current_prop_pos[data_element_id];	// position of the proposition to get
	int init_pos = _pos_;
	// checks if the processing is not finished
	if (_pos_ != -1 && _pos_ < number_of_props) {
		// check if it is the last prop
		if (_pos_ == (int) number_of_props - 1) {
			current_prop_pos[data_element_id]++;
			prop = props_table.back().back();
		} else {
			unsigned previous_props = 0;	// line sizes iterated until finding the position

			// iterates throught the table to get the correct position
			for (unsigned a = 0; a < props_table.size(); a++) {
				// check if the position is within the current line
				if (_pos_ < (int) props_table[a].size()) {
					// checks if the amount of propositions completed is bigger that the line sizes so far
					// enforces that the previous line is already processed (beta testing)
					if (current_prop_pos_completed[data_element_id] >= previous_props) {	// old, may not be working
					// if (current_prop_pos_completed[data_element_id] >= (previous_props - 1)) {	
						current_prop_pos[data_element_id]++;

						return props_table[a][_pos_];
					} else {
						// if not the thread has to wait for other threads to complete their propositions
						 // reportTable();
						// cout << "AQUI " << data_element_id << " " << current_prop_pos_completed[data_element_id] << " " << previous_props << " "<< _pos_ << " " << a << " " << props_table.size() << " " << props_table[a].size()<< " " << props_table[0].size() << " " << init_pos << " " << current_prop_pos_completed[data_element_id] << endl;
						// cout << "0\t" << "1\t" << "2\t" << "3\t" << "4\t" << "5\t" << "6\t" << "7\t" << "8\t" << "9\t" << "10\t" << "11\t" << "12\t" << "13\t" << "14\t" << "15\t" << "16\t" << "17\t" << endl;
						// for (unsigned jj = 0; jj < number_of_props; jj++)
						// 	cout << evs_props[data_element_id][jj] << "\t";
						// cout << endl << endl;
						return NO_PROPOSITION;
					}
				} else {
					// if not it subtracts the line size to the position to move to the next line
					_pos_ -= props_table[a].size();
					// positions (line sizes) iterated so far
					previous_props += props_table[a].size();
				}
			}
		}
	} else {
		// return an error code
		prop = NEXT_EVENT;
		return prop;
	}

	return prop;
}

bool Scheduler::passed (unsigned data_element_id) {
	pass_prop.lock();
	bool ps = (current_prop_pos[data_element_id] > -1);
	pass_prop.unlock();

	return ps;
}

int Scheduler::getCurrentPropPos (long unsigned data_element_id) {

	pass_prop.lock();
	unsigned current_pp = current_prop_pos[data_element_id];
	pass_prop.unlock();

	return current_pp;
}

bool Scheduler::passedAllProps (long unsigned data_element_id) {
	bool passed_all = false;

	pass_all_props.lock();
	if (current_prop_pos_completed[data_element_id] == number_of_props)
		passed_all = true;
	pass_all_props.unlock();

	return passed_all;
}


void Scheduler::updatePropsWeight (void) {

	for (unsigned i = 0; i < number_of_props; i++) {

		if (prop_passed[i]+prop_failed[i] == 0)
			prop_weight[i] = (log10(1+9*(prop_exec_time[i]/(float)max_prop_time))) * 0.3 + 0.7;
		else
			prop_weight[i] = (log10(1+9*(prop_exec_time[i]/(float)max_prop_time))) * 0.3 + 0.7 * (prop_passed[i]/(prop_passed[i]+prop_failed[i]));

	//	if (prop_weight[i] != 1)
	//		cout << i+1 <<": " << prop_weight[i] << "\t" << prop_exec_time[i] << endl;
	//	else
	//		cout << i+1 <<": " << prop_weight[i] << "\t\t" << prop_exec_time[i] << endl;
	}
	updatePropsTable();
}

void Scheduler::updatePropsTable (void) {

	// simple 30-60 
	for (unsigned i = 0; i < number_of_props; i++) {
		if (prop_weight[i] < 0.33 && props_queue[i] != 0 && props_table[props_queue[i]].size() > 1) {
			shiftUp(i);
		} else {
			if (prop_weight[i] < 0.66 && props_queue[i] != 1 && props_table[props_queue[i]].size() > 1) {
				if (props_queue[i] > 1){
					shiftUp(i);
				} else {
					shiftDown(i);
				}
			} else {
				if (props_queue[i] < 2 && props_table[props_queue[i]].size() > 1) {
					shiftDown(i);
				}
			}
		}
	}
	/*cout << endl;
	cout << "Proposition Table" << endl;

	for (auto v : props_table) {
		for (auto e : v)
			cout << e << " ";
		cout << endl << "------------------" << endl;
	}

	cout << endl << endl;*/
}

unsigned Scheduler::getNumThreads (void) {
	char hostname[100];
	gethostname(hostname, 100);
	string ab = "compute-002-1";
	string hn = hostname;

	if (!strcmp(ab.c_str(), hn.c_str())) {
		char* pPath;
		pPath = getenv ("HEPF_NUM_KNL_THREADS");

		if (pPath == NULL)
			return boost::thread::physical_concurrency();
		else
			return atoi (pPath);
	} else {
		char* pPath;
		pPath = getenv ("HEPF_NUM_THREADS");

		if (pPath == NULL)
			return boost::thread::physical_concurrency();
		else
			return atoi (pPath);
	}
}

unsigned Scheduler::getNumProcessThreads (void) {
	char* pPath;
	pPath = getenv ("HEPF_NUM_PROCESSERS");

	if (pPath == NULL)
		return 1;
	else
		return atoi (pPath);
}

unsigned Scheduler::getNumReaderThreads (void) {
	char* pPath;
	pPath = getenv ("HEPF_NUM_READERS");

	if (pPath == NULL)
		return 1;
	else
		return atoi (pPath);
}

void Scheduler::initialize (unsigned data_size) {


	number_of_elements			 = data_size;
	current_prop_pos			 = new int[data_size];
	current_prop_pos_completed	 = new unsigned[data_size];

	memset(current_prop_pos, 0, number_of_elements*sizeof(int));
	memset(current_prop_pos_completed, 0, number_of_elements*sizeof(unsigned));

	vector< vector<unsigned> > list_of_deps = createListDeps();
	createTable(list_of_deps);

	// evs_props = new unsigned* [number_of_elements];

	// for (unsigned i = 0; i < number_of_elements; i++) {
	// 	evs_props[i] = new unsigned[number_of_props];
	// 	memset(evs_props[i], 0, number_of_props*sizeof(unsigned));
	// }

	/*cout << endl;
	cout << "Proposition Table" << endl;

	for (auto v : props_table) {
		for (auto e : v)
			cout << e << " ";
		cout << endl << "------------------" << endl;
	}

	cout << endl << endl;*/
}
