#ifndef Scheduler_h
#define Scheduler_h

#include <chrono>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <queue>
#include <cmath>
#include <string.h>
#include <iomanip>
#include <boost/thread.hpp>
#include <unistd.h>

#define NO_PROPOSITION 9999
#define NEXT_EVENT 99999
#define BUFFER_UPDATE 0.3

using namespace std;

class Scheduler {
	unsigned number_of_props;
	unsigned number_of_elements;
	unsigned current_prop;
	long unsigned current_data;
	double max_prop_time;
	unsigned final_evs;

	double *prop_exec_time;
	long unsigned *prop_passed;
	long unsigned *prop_failed;
	float *prop_weight;

	unsigned **evs_props;
	boost::mutex pass_prop, pass_all_props;

	unsigned **props_deps;
	unsigned *props_start_deps;	// propositions in each chain of dependencies, first props will have value 1
	unsigned *props_end_deps;	// propositions in each chain of dependencies, first props will have value 1
	unsigned *props_queue;		// queue of each proposition from 0 to #queues
	vector <unsigned> *prop_up_neighbours;
	vector <unsigned> *prop_down_neighbours;

	vector< vector<unsigned> > props_table;	// holds the info about the props to execute in each stage
	int *current_prop_pos;
	unsigned *current_prop_pos_completed;

	vector< vector<unsigned> > createListDeps(void);
	void createTable (vector< vector<unsigned> > list_of_deps);
	void addUpNeighbours (unsigned prop, vector<unsigned> neighbours);
	void addDownNeighbours (unsigned prop, vector<unsigned> neighbours);
	void addUpNeighbour (unsigned prop, unsigned neighbour);
	void addDownNeighbour (unsigned prop, unsigned neighbour);
	bool shiftUp (unsigned prop);
	bool shiftDown (unsigned prop);
	void initBuffer (unsigned size);

public:
	Scheduler(void);
	Scheduler(unsigned n_propositions);
	~Scheduler(void);

	void passProposition(unsigned prop_id, unsigned data_element_id);
	void failProposition(unsigned prop_id, unsigned data_element_id);
	void failPropositionOld(unsigned prop_id);
	chrono::time_point<chrono::high_resolution_clock> propositionStart(void);
	void propositionStop(chrono::time_point<chrono::high_resolution_clock> init, unsigned prop_id);
	void setPropositionDep(unsigned prop1, unsigned prop2);
	unsigned getProposition(unsigned data_element_id);
	unsigned getPropositionOld(unsigned data_element_id);
	void initialize(unsigned data_size);
	bool passed (unsigned data_element_id);
	void report (void);
	void reportDebug (void);
	void updatePropsWeight (void);
	void updatePropsTable (void);
	unsigned getNumThreads (void);
	unsigned getNumProcessThreads (void);
	unsigned getNumReaderThreads (void);
	void reportTable (void);
	int getCurrentPropPos (long unsigned data_element_id);
	bool passedAllProps (long unsigned data_element_id);
};

#endif
