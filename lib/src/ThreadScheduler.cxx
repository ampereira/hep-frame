#include "ThreadScheduler.h"

using namespace std;



boost::condition_variable blocked_processers;
boost::condition_variable blocked_readers;

boost::mutex wait_blocked_processers;
boost::mutex wait_blocked_readers;

extern boost::thread *master_thread;
extern boost::thread *worker_thread;

int num_process_threads, num_active_process_threads;
int num_reader_threads, num_active_reader_threads;

ThreadScheduler::ThreadScheduler (void) {
	number_of_threads = 1;
	first_file_size	  = 1;
}

ThreadScheduler::ThreadScheduler (unsigned pthreads, unsigned rthreads) {
	finished_reading = false;

	number_of_threads	= pthreads;
	num_reader_threads	= rthreads;
	num_process_threads = pthreads;
	first_file_size	  = 1;

	active_reader_time	  = new double[number_of_threads];
	active_processer_time = new double[number_of_threads];

	data_per_thread = new long unsigned[number_of_threads];

	initial_active_reader_time	  = new chrono::time_point<chrono::high_resolution_clock>[number_of_threads];
	initial_active_processer_time = new chrono::time_point<chrono::high_resolution_clock>[number_of_threads];

	memset(active_reader_time, 0, number_of_threads*sizeof(double));
	memset(active_processer_time, 0, number_of_threads*sizeof(double));
	memset(data_per_thread, 0, number_of_threads*sizeof(long unsigned));

	num_active_reader_threads = (float)num_reader_threads/2.0 + 0.5;
	num_active_process_threads = number_of_threads - num_active_reader_threads;
}

int ThreadScheduler::initialize (unsigned pthreads, unsigned rthreads) {
	finished_reading = false;

	number_of_threads	= pthreads;
	num_reader_threads	= rthreads;
	num_process_threads = pthreads;

	active_reader_time	  = new double[number_of_threads];
	active_processer_time = new double[number_of_threads];

	data_per_thread = new long unsigned[number_of_threads];

	initial_active_reader_time	  = new chrono::time_point<chrono::high_resolution_clock>[number_of_threads];
	initial_active_processer_time = new chrono::time_point<chrono::high_resolution_clock>[number_of_threads];

	memset(active_reader_time, 0, number_of_threads*sizeof(double));
	memset(active_processer_time, 0, number_of_threads*sizeof(double));
	memset(data_per_thread, 0, number_of_threads*sizeof(long unsigned));

	if (rthreads > 1) {
		num_active_reader_threads  = (float)num_reader_threads/2.0 + 0.5;
		num_active_process_threads = num_process_threads - num_active_reader_threads;
	} else {
		num_active_process_threads = num_process_threads;
		num_active_reader_threads  = num_reader_threads;
	}

	return num_active_reader_threads;
}

ThreadScheduler::~ThreadScheduler (void) {
	delete active_reader_time;
	delete active_processer_time;
	delete initial_active_reader_time;
	delete initial_active_processer_time;
}

void ThreadScheduler::report (void) {
	cout << " => Thread scheduler report" << endl;
	cout << "------------------------------------------" << endl;
	cout << "Maximum # data processing threads:\t" << num_process_threads << endl;
	cout << "Maximum # data reading threads:\t\t" << num_reader_threads << endl;
	//cout << "num_reader_threads:\t\t" << num_reader_threads << endl;
	//cout << "num_active_reader_threads:\t" << num_active_reader_threads << endl;
	cout << "------------------------------------------" << endl;
	cout << endl << endl;
}

// returns time per dataset element
double ThreadScheduler::processAverage (void) {
	double result = 0;

	for (unsigned i = 0; i < num_active_process_threads; i++) {
		result += (active_processer_time[i] / (double) data_per_thread[i]);
	}

	return result;
}

// returns time per dataset element
double ThreadScheduler::readAverage (void) {
	double result = 0;

	for (unsigned i = 0; i < num_active_reader_threads; i++) {
		result += (active_reader_time[i] / (double) first_file_size);
	}

	return result;
}

void ThreadScheduler::updateActiveThreads (void) {
	double nt;
	double time_per_active_reader, time_per_active_processer;

	//cout << "before " <<num_active_process_threads << ", " << num_active_reader_threads << endl;

	if (!finished_reading) {
		// if DSt < DPt, DXtt is the time per thread
		//cout << active_reader_time[0] << " - " << active_processer_time[0] << endl;

		if (active_reader_time[0] <= active_processer_time[0]) {
			time_per_active_reader = readAverage() / (double) num_active_reader_threads;

			// REMEMBER: nt is a negative value
			nt = (readAverage() - processAverage()) / (2.0 * time_per_active_reader);
			
			int _nt = nt;
			updateActiveReaders(_nt);
			_nt = _nt * (-1);
			updateActiveProcessers(_nt);
		} else {
			time_per_active_processer = processAverage() / (double) num_active_process_threads;

			nt = (processAverage() - readAverage()) / (2.0 * time_per_active_processer);

			int _nt = nt;
			updateActiveProcessers(_nt);
			_nt = _nt * (-1);
			updateActiveReaders(_nt);
		}

		// reset the timers
		memset(active_reader_time, 0, number_of_threads*sizeof(double));
		memset(active_processer_time, 0, number_of_threads*sizeof(double));
		memset(data_per_thread, 0, number_of_threads*sizeof(long unsigned));
	} else {
		updateActiveProcessers(number_of_threads);
	}

	//cout << "nt " << nt << endl;
	//cout << "after " <<num_active_process_threads << ", " << num_active_reader_threads << endl <<endl;
}

// update +-x to the number of threads
int ThreadScheduler::updateActiveReaders (int step) {

	num_active_reader_threads += step;

	if (num_active_reader_threads <= 0)
		num_active_reader_threads = 1;

	if (num_active_reader_threads > num_reader_threads)
		num_active_reader_threads = num_reader_threads;

	{
		boost::unique_lock<boost::mutex> _lock (wait_blocked_readers);
		blocked_readers.notify_all();
	}

	return num_active_reader_threads;
}

// update +-x to the number of threads
int ThreadScheduler::updateActiveProcessers (int step) {

	num_active_process_threads += step;

	if (num_active_process_threads <= 0)
		num_active_process_threads = 1;

	if (num_active_process_threads > num_process_threads)
		num_active_process_threads = num_process_threads;

	if (num_active_process_threads < num_process_threads - num_reader_threads)
		num_active_process_threads = num_process_threads - num_reader_threads;

	{
		boost::unique_lock<boost::mutex> _lock (wait_blocked_processers);
		blocked_processers.notify_all();
	}

	return num_active_process_threads;
}

void ThreadScheduler::processerWait (void) {
	boost::unique_lock<boost::mutex> _lock (wait_blocked_processers);
	blocked_processers.wait(_lock);
}

void ThreadScheduler::readerWait (void) {
	boost::unique_lock<boost::mutex> _lock (wait_blocked_readers);
	blocked_readers.wait(_lock);
}

void ThreadScheduler::timeStartRead (unsigned thread_id) {
	initial_active_reader_time[thread_id] = chrono::high_resolution_clock::now();
}

void ThreadScheduler::timeStopRead (unsigned thread_id) {
	auto end_time = chrono::high_resolution_clock::now();

	chrono::duration<double> elapsed = end_time - initial_active_reader_time[thread_id];

	active_reader_time[thread_id] += elapsed.count() * 10000000;	// to nanosseconds
}

void ThreadScheduler::timeStartProcess (unsigned thread_id) {
	initial_active_processer_time[thread_id] = chrono::high_resolution_clock::now();
}

void ThreadScheduler::timeStopProcess (unsigned thread_id) {
	auto end_time = chrono::high_resolution_clock::now();

	chrono::duration<double> elapsed = end_time - initial_active_processer_time[thread_id];

	active_processer_time[thread_id] += elapsed.count() * 10000000;	// to nanosseconds

	data_per_thread[thread_id]++;
}

void ThreadScheduler::setFileSize (long unsigned fs) {
	first_file_size = fs;
}

int ThreadScheduler::unlockReaders (void) {
	finished_reading = true;

	return updateActiveReaders(number_of_threads);
}

int ThreadScheduler::unlockProcessers (void) {
	return updateActiveProcessers(number_of_threads);
}
