#ifndef PROGRAMOPTIONS_h
#define PROGRAMOPTIONS_h

#include <sstream>
#include <iostream>
#include <string>
#include <map>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <boost/program_options.hpp>
#include <dirent.h>
#include <vector>

using namespace std;
namespace po = boost::program_options;

class ProgramOptions {
protected:
	po::options_description desc;
	po::variables_map vm;

public:
	ProgramOptions (void);
	template<typename T>
	void add (string name, string short_name, string description, bool required);
	template<typename T>
	void add (string name, string description, bool required);
	bool getOptions (int argc, char *argv[], map<string, string> &options, vector<string> &file_list);
};

#endif
