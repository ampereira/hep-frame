#ifndef DATAANALYSIS_h
#define DATAANALYSIS_h

#include <chrono>
#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <algorithm>
#include <unistd.h>

#ifdef D_ROOT
	#include <TFile.h>
	#include <TTree.h>
	#include <TChain.h>
	#include <TRandom3.h>
#endif

#include <fstream>
#include "DataReader.h"
#include "Timer.h"
#include "ProgramOptions.h"
#include "PseudoRandomGenerator.h"
#include "Event.h"
#include "TaskStealingScheduler.h"
#include "ThreadScheduler.h"
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/lockfree/queue.hpp>

#ifdef D_MPI
	#include <mpi.h>
#endif

#define NUM_THREADS 1
#define CUT_CHECKPOINT 100

typedef bool (*Cut) (unsigned this_event_counter);

using namespace std;

class DataAnalysis {
protected:
	int process_id;
	int number_of_processes;

	vector<string> root_file_list;
	string output_root_file;
	string tree_filename;
	string tree_name;
	string signal_file;
	string background_file;
	Timer t;

	map<unsigned, Cut> cuts;	// map with name and function pointers for all cuts
	map<string, unsigned> cut_table;	// cut name to code
	map<unsigned, string> cut_table_inverted;	// cut code to name
	unsigned cut_counter;	// different from number of cuts, it's used for the matrix and starts at 1
	Scheduler *sched = NULL;
	ThreadScheduler *thread_scheduler = NULL;
	vector<unsigned> cuts_passed;	//amount of cuts that each event passed

	vector<unsigned> *this_thread_cuts_passed;

	//#ifdef D_ROOT
	//TTree **tree_rec_vars;
	//TTree *to_write;
	//#endif

	bool clone_tree = false;

	bool save_events;

	// Counters
	unsigned number_of_cuts;
	bool prop_updating = false;

	unsigned *passed_each_cut;

	long unsigned last_sched_update = 0;

	long number_events;


	Timer io;
	Timer compute;
	Timer output;
	float io_time, compute_time, output_time;

	unsigned cut_to_save_events = 10000;


	string hostname;
	string knl = "compute-002-1";


	void loop (unsigned td);
	void loopOpenMP (void);		//deprecated
	void loopRegularScheduler (unsigned td);
	void processCuts (void);
	void loopSeq (void);	//deprecated
	void processCutsNoRecord (void);
	void writeAllHistograms (void);
	void report (void);
	void reportSeq (void);
	void writeEvents (void);
	void initCuts (void);
	void initCutDependencies (void);
	void updateCutWeight (unsigned cut_number);
	void computeCutOrder (void);
	void reorderCuts (void);
	void loadEvents (unsigned td);
	void loadEventsStatic (unsigned td);
	void progressBar (float progress);



	virtual void recordVariables (unsigned cut_number, long unsigned this_event_counter);
	virtual void recordVariablesBoost (long unsigned event);
	virtual void writeVariables (void);
	virtual void initRecord (unsigned);
	virtual void initialize (void);
	virtual void finalize (void);
	virtual void writeResults (void);
	virtual void fillHistograms (string cut_name, long unsigned this_event_counter);


public:
	DataAnalysis (vector<string> _rootfile_list, string _tree_filename, string _output_root_file, string _signal_file, string _background_file, string _tree_name, unsigned ncuts);
	DataAnalysis (vector<string> _rootfile_list, string _tree_filename, string _output_root_file, string _signal_file, string _background_file, unsigned ncuts);
	~DataAnalysis (void);
	void run (void);
	void runByDefines (void);
	void runByName (void);
	void runSeq (void);
	unsigned addCut (string name, Cut c);
	bool addCutDependency (string cut1, string cut2);
	long unsigned currentEvent (void);
	//void readPdf (vector<string> signals, vector<string> backgrounds);

	void testPRNG (void);
	void consume (void);
	void produce (void);
	void gpuPrngProducer (unsigned tid);
	void produceKNC (unsigned tid);

	// debug
	void printCuts (void);
	void printCutsOrder (void);

	virtual void coisas (void);
};

#endif
