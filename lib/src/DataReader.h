#ifndef DATAREADER_h
#define DATAREADER_h

#include <string>
#include <iostream>
#include <vector>

#ifdef D_ROOT
	#include <TROOT.h>
	#include <TChain.h>
	#include <TFile.h>
	#include <TH1D.h>
	#include <TH2D.h>
	#include <TH3D.h>
	#include <TKey.h>
#endif

#include "Event.h"
#include <chrono>
#include <boost/thread.hpp>
#include <map>
#include <sstream>

using namespace std;

class DataReader {
	int fCurrent;	//!current Tree number in a TChain
	long number_events;
	long start_structure_events;
	string filepath;

	#ifdef D_ROOT
	TFile *file;
	TTree *fChain = NULL;	//!pointer to the analyzed TTree or TChain

	
	void loadHistograms (void);
	int getEntry (long entry);
	long loadTree (long entry);
	#endif

	void progressBar (float progress);

public:
	DataReader (string filepath, string tree_name);
	DataReader (string filepath);
	DataReader (void);
	~DataReader (void);
	void loadEvents (void);
	long numberEvents (void);
	void setStartEvents(long);
	void writeEvents (vector<unsigned> cuts_passed, string output_root_file);

	#ifdef D_ROOT
	TTree* getfChain (void);
	#endif
};

#endif
