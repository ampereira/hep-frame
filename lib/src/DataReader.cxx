#include "DataReader.h"

#ifdef D_DELPHES
	#include <ExRootAnalysis/ExRootAnalysis/ExRootTreeReader.h>

	extern ExRootTreeReader *ex_tree;
	extern TTree *tree;

	extern TClonesArray *branchJet;
	extern TClonesArray *branchElectron;
	extern TClonesArray *branchPhoton;
	extern TClonesArray *branchMuon;
	extern TClonesArray *branchMissingET;
	extern TClonesArray *branchScalarHT;
	extern TClonesArray *branchGenParticle;
	extern TClonesArray *branchTrack;
	extern TClonesArray *branchTower;
	extern TClonesArray *branchLHCOEvent;
	extern TClonesArray *branchLHEFEvent;
	extern TClonesArray *branchHepMCEvent;
	extern TClonesArray *branchEvent;
#endif

using namespace std;

HEPEvent *events = NULL;

#ifdef D_ROOT
map<string, map <string, TH1D*>> th1d;
map<string, map <string, TH2D*>> th2d;
map<string, map <string, TH3D*>> th3d;
#endif

extern long current_number_events;
extern long total_number_events;
extern bool thread_blocked;
extern boost::condition_variable wait_for_event_load2;
extern boost::mutex wait_for_event_load_mt2;
boost::mutex update_event_count;
boost::mutex load_histos;


// DISCLAIMER: ROOT file needs a TTree
DataReader::DataReader (string _filepath, string tree_name) {
	start_structure_events = 0;
	filepath = _filepath;

	#ifdef D_ROOT
	file = (TFile*)gROOT->GetListOfFiles()->FindObject(filepath.c_str());

	if (!file || !file->IsOpen()) {
		file = new TFile(filepath.c_str());
	} else {
		cerr << "Error: " << filepath << " does not exist or is not accessible!" << endl;
		exit(-1);
	}

	file->GetObject(tree_name.c_str(), fChain);

	if (!fChain) {
		cerr << "Error: Problem in the root file structure" << endl;
		cerr << "Error: TTree " << fChain->GetName() << endl;
		exit (-1);
	}

	fCurrent = -1;
	fChain->SetMakeClass(1);
	number_events = fChain->GetEntriesFast();
	#endif
}

DataReader::DataReader (string _filepath) {
	start_structure_events = 0;
	filepath = _filepath;


	#ifdef D_ROOT
	file = (TFile*)gROOT->GetListOfFiles()->FindObject(filepath.c_str());

	if (!file || !file->IsOpen()) {
		file = new TFile(filepath.c_str());
	} else {
		cerr << "Error: " << filepath << " does not exist or is not accessible!" << endl;
		exit(-1);
	}

	TIter nextkey (file->GetListOfKeys());
	TKey *key;
	bool tree_found = false;

	while ((key = (TKey*) nextkey()) && !tree_found) {
		TObject *obj = key->ReadObj();
		if (obj->IsA()->InheritsFrom(TTree::Class())) {
			fChain = (TTree*)obj;
			tree_found = true;
		}
	}

	if (!fChain) {
		cerr << "Error: Problem in the root file structure" << endl;
		cerr << "Error: TTree " << fChain->GetName() << endl;
		exit (-1);
	}

	#ifndef D_DELPHES
	fCurrent = -1;
	fChain->SetMakeClass(1);
	number_events = fChain->GetEntriesFast();
	#endif

	#endif
}

// nao funciona por causa do fchain e nao faco ideia porque
void DataReader::writeEvents (vector<unsigned> cuts_passed, string output_root_file) {

	#ifdef D_VERBOSE
	cout << endl << flush;
	cout << "Writing events" << endl << endl;
	long unsigned last_n = 0;
	progressBar(0.0);
	#endif

	TFile *out_file = new TFile (output_root_file.c_str(), "recreate");

	TTree *new_to_write = fChain->CloneTree(0);

	long unsigned number_events_cuts_passed = cuts_passed.size();

	for (long unsigned i = 0; i < number_events_cuts_passed; i++) {
		#ifdef D_VERBOSE
		if (i == (number_events_cuts_passed - 1)) {
			progressBar(1);
			cout << endl << endl << "All events wrote" << endl << endl;
		} else if (i >= last_n + 2000) {
			float n = (float)i/(float)number_events_cuts_passed;
			last_n = i;
			progressBar(n);
		}
		#endif

		events[cuts_passed[i]].loadEvent(cuts_passed[i]);
		events[cuts_passed[i]].write(new_to_write);
	}

	out_file->Write();
	out_file->Close();
}

#ifdef D_ROOT
TTree* DataReader::getfChain (void) {
	return fChain;
}
#endif

void DataReader::setStartEvents(long st) {
	start_structure_events = st;
}

long DataReader::numberEvents (void) {
	return number_events;
}

DataReader::DataReader (void) {

}

DataReader::~DataReader (void) {

	//if (!fChain)
	//	return;

	//delete fChain->GetCurrentFile();	//comentado so para o TTH
}

#ifdef D_ROOT
// Only goes 1 directory deep
void DataReader::loadHistograms (void) {

	TIter nextkey (file->GetListOfKeys());
	TKey *key;

	while ((key = (TKey*) nextkey())) {
		TObject *obj = key->ReadObj();

		if (obj->IsA()->InheritsFrom(TDirectory::Class())) {
			TDirectory* ccc = (TDirectory*) obj;

			TIter nextkey2 (ccc->GetListOfKeys());

			while ((key = (TKey*) nextkey2())) {
				TObject *obj2 = key->ReadObj();

				if (obj2->IsA()->InheritsFrom(TH1D::Class())) {
					stringstream name, name2;
					name << ccc->GetName();
					name2 << obj2->GetName();

					th1d[name.str()][name2.str()] = (TH1D*)obj2;
				} else if (obj2->IsA()->InheritsFrom(TH2D::Class())) {
					stringstream name, name2;
					name << ccc->GetName();
					name2 << obj2->GetName();

					th2d[name.str()][name2.str()] = (TH2D*)obj2;
				} else if (obj2->IsA()->InheritsFrom(TH3D::Class())) {
					stringstream name, name2;
					name << ccc->GetName();
					name2 << obj2->GetName();

					th3d[name.str()][name2.str()] = (TH3D*)obj2;
				}
			}
		}

		if (obj->IsA()->InheritsFrom(TH1D::Class())) {
			stringstream name, name2;
			name << "root";
			name2 << obj->GetName();

			th1d[name.str()][name2.str()] = (TH1D*)obj;
		} else if (obj->IsA()->InheritsFrom(TH2D::Class())) {
			stringstream name, name2;
			name << "root";
			name2 << obj->GetName();

			th2d[name.str()][name2.str()] = (TH2D*)obj;
		} else if (obj->IsA()->InheritsFrom(TH3D::Class())) {
			stringstream name, name2;
			name << "root";
			name2 << obj->GetName();

			th3d[name.str()][name2.str()] = (TH3D*)obj;
		}
	}

	// for(auto const &ent1 : th1d) {
	//   // ent1.first is the first key
	//   for(auto const &ent2 : ent1.second) {
	//     // ent2.first is the second key
	//     // ent2.second is the data
	//     cout << ent1.first << " - " << ent2.first << endl;
	//   }
	// }
	// th1d["mujets_2017_DL1"]["cutflow_Loose"]->Print();

	if (!fChain) {
		cerr << "Error: Problem in the root file structure" << endl;
		cerr << "Error: TTree " << fChain->GetName() << endl;
		exit (-1);
	}
}

// Read contents of entry
int DataReader::getEntry (long entry) {

	if (!fChain)
		return 0;

	return fChain->GetEntry(entry);
}

// Set the environment to read one entry
long DataReader::loadTree (long entry) {

	if (!fChain)
		return -5;

	long centry = fChain->LoadTree(entry);

	if (centry < 0)
		return centry;

	if (fChain->GetTreeNumber() != fCurrent)
		fCurrent = fChain->GetTreeNumber();

	return centry;
}
#endif

// Load all events into memory
#ifdef D_DELPHES
void loadBranches (void) {

	// Get pointers to branches used in this analysis
	branchJet       = ex_tree->UseBranch("Jet");
	if (branchJet==NULL) cout << "Jet collection branch is not found" << endl;

	branchElectron  = ex_tree->UseBranch("Electron");
	if (branchElectron==NULL) cout << "Electron collection branch is not found" << endl;

	branchPhoton    = ex_tree->UseBranch("Photon");
	if (branchPhoton==NULL) cout << "Photon collection branch is not found" << endl;

	branchMuon      = ex_tree->UseBranch("Muon");
	if (branchMuon==NULL) cout << "Muon collection branch is not found" << endl;

	branchMissingET = ex_tree->UseBranch("MissingET");
	if (branchMissingET==NULL) cout << "MissingEt branch is not found" << endl;

	branchScalarHT = ex_tree->UseBranch("ScalarHT");
	if (branchScalarHT==NULL) cout << "ScalarHT branch is not found" << endl;

	branchGenParticle = ex_tree->UseBranch("Particle");
	if (branchGenParticle==NULL) cout << "GenParticle branch is not found" << endl;

	branchTrack = ex_tree->UseBranch("Track");
	if (branchTrack==NULL) cout << "Track branch is not found" << endl;

	branchTower = ex_tree->UseBranch("Tower");
	if (branchTower==NULL) cout << "Tower branch is not found" << endl;
}

void DataReader::loadEvents (void) {

	ex_tree = new ExRootTreeReader(fChain);

	loadBranches();

	long unsigned nevents = ex_tree->GetEntries();
	number_events = nevents;


	for (long unsigned i = 0; i < nevents; i++) {
		HEPEvent myEvent;

		ex_tree->ReadEntry(i);

		myEvent.loadEvent();

		events.push_back(myEvent);
	}
}
#endif

void DataReader::progressBar (float progress) {
	// float progress = 0.0;
	// while (progress < 1.0) {
	int barWidth = 70;
	
	// cout << '\r' << std::flush;
	std::cout << "[";
	int pos = barWidth * progress;
	for (int i = 0; i < barWidth; ++i) {
		if (i < pos) std::cout << "=";
		else if (i == pos) std::cout << ">";
		else std::cout << " ";
	}
	int pp = (progress * 100.0);

	if (pp > 100)
		pp = 100;

	std::cout << "] " << pp << " %\r";
	std::cout.flush();

	// 	progress += 0.16; // for demonstration only
	// }
}

#ifdef D_ROOT
void DataReader::loadEvents (void) {
	bool loading = true;
	long since_last = 0;
	long last_n = 0;
	long nn = 0;

	load_histos.lock();
	loadHistograms();
	load_histos.unlock();

	for (long event_id = 0; event_id < number_events && loading; event_id++) {

		// cout << n << endl;
		#ifdef D_VERBOSE
		if ((start_structure_events + event_id) == (total_number_events - 1)) {
			progressBar(1);
			cout << endl;
			cout.flush();
			cout << endl << endl << "All events loaded" << endl << endl;
		} else if (event_id >= last_n + 2000) {
			float n = (float)(start_structure_events + event_id)/(float)total_number_events;
			// cout << n << endl;
			last_n = event_id;
			progressBar(n);
		}
		#endif

		long tentry = loadTree(event_id);

		

		if (tentry < 0) {
			cerr << "Error: Could not load all entries on the input file" << endl;
			cerr << "Error: Entry " << tentry << " in the " << fChain->GetName() << " TTree" << endl;

			loading = false;
		} else {
			// Creates and loads an event into memory
			events[start_structure_events] = HEPEvent (fChain);
			events[start_structure_events].init();
			events[start_structure_events].loadEvent(event_id);

			start_structure_events++;
			since_last++;

			// cout << event_id << "\t" << number_events << endl;

			// a dar problemas com o loopRegularScheduler
			if (thread_blocked) {
				update_event_count.lock();
				current_number_events += since_last;
				update_event_count.unlock();

				since_last = 0;

				thread_blocked = false;
				// boost::unique_lock<boost::mutex> _lock (wait_for_event_load_mt2);
				// wait_for_event_load2.notify_all();
			}
		}
	}
	update_event_count.lock();
	current_number_events += since_last;
	update_event_count.unlock();
}
#endif


