#ifndef ThreadScheduler_h
#define ThreadScheduler_h

#include <chrono>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <boost/chrono.hpp>
#include <boost/thread.hpp>

using namespace std;

class ThreadScheduler {
	bool finished_reading;
	unsigned number_of_threads;
	unsigned number_of_files;
	long unsigned first_file_size;
	long unsigned *data_per_thread;
	double *active_reader_time, *active_processer_time;
	chrono::time_point<chrono::high_resolution_clock> *initial_active_reader_time, *initial_active_processer_time;

	double processAverage (void);
	double readAverage (void);

public:
	ThreadScheduler(void);
	ThreadScheduler(unsigned pthreads, unsigned rthreads);
	int initialize (unsigned pthreads, unsigned rthreads);
	~ThreadScheduler(void);
	void timeStopRead (unsigned thread_id);
	void timeStartRead (unsigned thread_id);
	void timeStopProcess (unsigned thread_id);
	void timeStartProcess (unsigned thread_id);
	void setFileSize (long unsigned fs);
	int updateActiveReaders (int step);
	int updateActiveProcessers (int step);
	void updateActiveThreads (void);
	int unlockReaders (void);
	int unlockProcessers (void);
	void processerWait (void);
	void readerWait (void);
	void report (void);
};

#endif
