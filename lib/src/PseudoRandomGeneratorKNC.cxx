#include "PseudoRandomGenerator.h"
#define MAX_PRNS 50000

double PseudoRandomGenerator::gaussianMKLKNC (void) {
	double val;
	bool last_update = false;

	#ifdef D_HEPF_INTEL
	#ifdef D_KNC
	if (which_mkl == true) {
		get_mkl_prn.lock();
		val = mkl_prns2[current_mkl_prn++];
	// cout << which_mkl << ":\t" << current_mkl_prn << endl;

		if ((current_mkl_prn > (MAX_PRNS * 0.9)) && last_update != which_mkl ){
			boost::unique_lock<boost::mutex> _lock (wait_mkl_mt);
			wait_mkl.notify_all();
			last_update = which_mkl;
		}

		if (current_mkl_prn == MAX_PRNS) {	
			which_mkl = 1 - which_mkl;
			current_mkl_prn = 0;
		}

		get_mkl_prn.unlock();
	} else {
		get_mkl_prn.lock();
		val = mkl_prns1[current_mkl_prn++];
	// cout << which_mkl << ":\t" << current_mkl_prn << endl;

		if ((current_mkl_prn > (MAX_PRNS * 0.9)) && last_update != which_mkl ) {
			boost::unique_lock<boost::mutex> _lock (wait_mkl_mt);
			wait_mkl.notify_all();
			last_update = which_mkl;
		}

		if (current_mkl_prn == MAX_PRNS) {	
			which_mkl = 1 - which_mkl;
			current_mkl_prn = 0;
		}

		get_mkl_prn.unlock();
	}
	#endif
	#endif

	return val;
}

void PseudoRandomGenerator::MKLArrayProducerKNC (void) {
	#ifdef D_HEPF_INTEL
	#ifdef D_KNC
	int brng = VSL_BRNG_MT19937;
	int errcode;
	int method = VSL_RNG_METHOD_GAUSSIAN_BOXMULLER;
	int nn = MAX_PRNS;
	int seed = time(NULL);
    VSLStreamStatePtr stream;

	while (!shutdown_prn) {

		if (which_mkl == true){
			#pragma offload target(mic) in(brng, seed, method, nn, p1, p2) out(mkl_prns1: length(nn)) nocopy(stream)
		    {
		        /***** Initialize *****/
		        errcode = vslNewStream( &stream, brng, seed );
		        CheckVslError( errcode );

		        /***** Call RNG *****/
		        errcode = vdRngGaussian( method, stream, nn, mkl_prns1, p1, p2 );
		        CheckVslError( errcode );

		        /***** Deinitialize *****/
		        errcode = vslDeleteStream( &stream );
		        CheckVslError( errcode );
		    }
			// #pragma offload target(mic:0) in(mkl_stream, p1, p2) inout(mkl_prns1: length(MAX_PRNS))
			// {
			// 	vdRngGaussian(VSL_RNG_METHOD_GAUSSIAN_BOXMULLER, mkl_stream, MAX_PRNS, mkl_prns1, p1, p2);
			// }

			// generate_mkl = true;
			current_prn = 0;
			// cout << endl<<"GEROU PRNS1" << endl << endl;
		} else {
			#pragma offload target(mic) in(brng, seed, method, p1, p2) out(mkl_prns2: length(nn)) nocopy(stream)
		    {
		        /***** Initialize *****/
		        errcode = vslNewStream( &stream, brng, seed );
		        CheckVslError( errcode );

		        /***** Call RNG *****/
		        errcode = vdRngGaussian( method, stream, nn, mkl_prns2, p1, p2 );
		        CheckVslError( errcode );

		        /***** Deinitialize *****/
		        errcode = vslDeleteStream( &stream );
		        CheckVslError( errcode );
		    }
			// #pragma offload target(mic:0) in(mkl_stream, p1, p2) inout(mkl_prns2: length(MAX_PRNS))
			// {
			// 	vdRngGaussian(VSL_RNG_METHOD_GAUSSIAN_BOXMULLER, mkl_stream, MAX_PRNS, mkl_prns2, p1, p2);
			// }
			// generate_mkl = true;
			current_prn = 0;
			// cout << endl<<"GEROU PRNS2" << endl << endl;
		}

		generated_mkl = true;

		{
			boost::unique_lock<boost::mutex> _lock (wait_mkl_mt);
			wait_mkl.wait(_lock);
		}
	}
	#endif
	#endif
}