#include "DataAnalysis.h"

#define SCHEDULER_UPDATE 1000
#define EVENT_CHUNK 20

using namespace std;

extern HEPEvent *events;
map<string, unsigned> thread_ids;

PseudoRandomGenerator rnd;

unsigned *nfiles = NULL;

long unsigned event_counter;
unsigned thread_id;

extern int num_process_threads, num_active_process_threads;
extern int num_reader_threads, num_active_reader_threads;

extern boost::mutex *record_vars;

// these two are used by datareader
long current_number_events;
long total_number_events;


unsigned thread_update;



boost::thread *master_thread = NULL;
boost::thread *worker_thread = NULL;
boost::barrier *bar_loading = NULL;
boost::barrier *bar_after_load = NULL;

boost::condition_variable wait_for_queue_pass;
boost::condition_variable wait_for_event_load;
boost::condition_variable wait_for_event_load2;


boost::mutex passed, passed3, failed, start_time, final, stop_time, id, get_prop, wait_for_queue_pass_mt, get_prop2, wait_for_event_load_mt, wait_for_event_load_mt2;
boost::mutex ev_init, load_events_update;

boost::mutex *fill_histograms;

bool thread_blocked = false;
bool finished_reading = false;


boost::lockfree::queue<DataReader*> root_reader(1);
boost::lockfree::queue<DataReader*> root_read(1);

/*
 * DISCLAIMER => cuts begin with id 1 to number_of_cuts (including this value)
 */

DataAnalysis::DataAnalysis (vector<string> _rootfile_list, string _tree_filename, string _output_root_file, string _signal_file, string _background_file, string _tree_name, unsigned ncuts) {
	#ifdef D_MPI
		MPI_Comm_rank(MPI_COMM_WORLD, &process_id);
		MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
	#else
		process_id = 0;
		number_of_processes = 1;
	#endif

	event_counter = 0;
	tree_name = _tree_name;
	root_file_list = _rootfile_list;
	stringstream ts1;
	ts1 << _output_root_file;
	ts1 << "_" << process_id << ".root";
	output_root_file = ts1.str();
	stringstream ts2;
	ts2 << _tree_filename;
	ts2 << "_" << process_id << ".root";
	tree_filename = ts2.str();
	signal_file = _signal_file;
	background_file = _background_file;
	//pdfs = false;
	number_of_cuts = ncuts;

	initCuts();
}

DataAnalysis::DataAnalysis (vector<string> _rootfile_list, string _tree_filename, string _output_root_file, string _signal_file, string _background_file, unsigned ncuts) {
	#ifdef D_MPI
		MPI_Comm_rank(MPI_COMM_WORLD, &process_id);
		MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
	#else
		process_id = 0;
		number_of_processes = 1;
	#endif

	event_counter = 0;
	tree_name = "unset";
	root_file_list = _rootfile_list;
	stringstream ts1;
	ts1 << _output_root_file;
	ts1 << "_" << process_id << ".root";
	output_root_file = ts1.str();
	stringstream ts2;
	ts2 << _tree_filename;
	ts2 << "_" << process_id << ".root";
	tree_filename = ts2.str();
	signal_file = _signal_file;
	background_file = _background_file;
	//pdfs = false;
	number_of_cuts = ncuts;

	initCuts();
}

DataAnalysis::~DataAnalysis (void) {
	//delete[] tree_rec_vars;
	//if (events)
	//	delete events;
}

void DataAnalysis::initCuts (void) {
	cut_counter   = 1;

	sched = new Scheduler(number_of_cuts);
	thread_scheduler = new ThreadScheduler();


	//tree_rec_vars = new TTree*[number_of_cuts];

	//for (unsigned i = 0; i < number_of_cuts; i++){
	//	stringstream s1, s2;
	//	s1 << "Recorded_Vars_";
	//	s1 << i;
	//	s2 << "Variables recorded for cut ";
	//	s2 << i;

	//	tree_rec_vars[i] = new TTree (s1.str().c_str(), s2.str().c_str());
	//}

	fill_histograms = new boost::mutex[number_of_cuts];

	current_number_events = 0;

	char hn[100];
	gethostname(hn, 100);
	hostname = hn;

	// cout << "HOSTNAME " << hostname << endl;
}

void DataAnalysis::coisas(void) {
	cout << "coisas"<< endl;
}

void DataAnalysis::loopOpenMP (void) {
	/*
	long unsigned number_events = events.size();

	cout << "Dataset size: " << number_events * sizeof(HEPEvent)/1048576 << " MBytes" << endl;



	event_counter = 0;

	#pragma omp parallel shared(event_counter)
	{
		#ifdef D_MULTICORE
		thread_id = omp_get_thread_num();
		num_process_threads = omp_get_num_process_threads();
		#else
		thread_id = 0;
		num_process_threads = 1;
		#endif

		#pragma omp single
		initRecord();

		bool pass = true;
		unsigned prop_id = 0;

		// versao paralela com openmp
		#pragma omp for schedule(dynamic) nowait
		for (unsigned t = 0; t < num_process_threads; t++) {
			unsigned amount_props = 0;

			while (event_counter < number_events) {
				prop_id = sched->getProposition(event_counter);

				if (prop_id != NO_PROPOSITION) {

					Cut c = cuts[prop_id+1];

					sched->propositionStart(prop_id);

					pass = c();

					sched->propositionStop(prop_id);

					if (pass && sched->passed(event_counter)) {
						// ver se isto ta certo (e prop_id?)
						#pragma omp critical
						{
							recordVariables(prop_id);
							cuts_passed[event_counter]++;
						}

						sched->passProposition(prop_id, event_counter);
					} else {
						sched->failProposition(prop_id, event_counter);
						amount_props = 0;

						#pragma omp critical
						{
							event_counter++;
						}
					}

					// increments the event counter in case that it is tha last prop
					if (cuts_passed[event_counter] == number_of_cuts) {
						#pragma omp critical
						{
							event_counter++;
						}
					}
				}

				if (event_counter % 100 == 0 && thread_id == 0)
					sched->updatePropsWeight();
			}
		}
	}

	event_counter++;
	*/
}

void DataAnalysis::loopRegularScheduler (unsigned td) {
	unsigned this_thread_id = td;

	#ifdef D_AFFINITY
	cpu_set_t cpuset;
	CPU_ZERO( & cpuset);
	CPU_SET( td, & cpuset);

	int erro(::pthread_setaffinity_np( ::pthread_self(), sizeof( cpuset), & cpuset));
	#endif

	// ofstream myfile;

	get_prop2.lock();
	string threadId = boost::lexical_cast<std::string>(boost::this_thread::get_id());
	thread_ids.insert(pair<string, unsigned>(threadId, this_thread_id));
	// string ff = "events_regular_scheduler_" + to_string(this_thread_id);
	// ff +=  ".txt";
	// myfile.open (ff.c_str());
	get_prop2.unlock();
	


	long _number_events = number_events;
	unsigned this_event_counter;

	unsigned prop_id = 0;

	unsigned this_thread_passed_each_cut [number_of_cuts];


	memset(this_thread_passed_each_cut, 0, number_of_cuts * sizeof(unsigned));
	// memset(this_thread_cuts_passed    , 0, _number_events * sizeof(unsigned));

	while (event_counter < _number_events) {
		if (this_thread_id < num_active_process_threads) {

			get_prop.lock();
			this_event_counter = event_counter;
			event_counter += EVENT_CHUNK;
			get_prop.unlock();

			
			for (int j = 0; j < EVENT_CHUNK; j++) {

				if ((this_event_counter >= _number_events) && finished_reading) {
					j = EVENT_CHUNK;
				} else {
						
					// checks if there are events to process in the file
					if ((this_event_counter >= current_number_events) && !finished_reading) {
						thread_blocked = true;
						j--;

						boost::this_thread::sleep_for( boost::chrono::microseconds(50) );//barriers
					} else {
						// measure time for the thread balancing
						thread_scheduler->timeStartProcess(this_thread_id);

						bool pass = true;
						Cut c;
						// cout << this_thread_id << " - " << this_event_counter << endl;

						for (unsigned i = 1; i <= number_of_cuts && pass; i++) {
							c = cuts[i];

							pass = c(this_event_counter);

							fill_histograms[i - 1].lock();
							fillHistograms(cut_table_inverted[i], this_event_counter);
							fill_histograms[i - 1].unlock();


							if (pass) {
								// cout << this_thread_id << "\t"<< i - 1 << "\t" << this_event_counter << endl;
								
								// gravar cenas
							//	record_vars[i-1].lock();
								recordVariables(i-1, this_event_counter);
								this_thread_passed_each_cut[i-1]++;


								
							//	record_vars[i-1].unlock();
							}
						}

						// cout << "coisas "<< number_of_cuts<< endl;
						// passed.lock();
						// if (pass) {
						// 	cuts_passed.push_back(this_event_counter);
						// }
						// passed.unlock();
	// cout << this_thread_id << endl;
						if (cut_to_save_events == 10000 && pass)
							this_thread_cuts_passed[this_thread_id].push_back(this_event_counter);
							// cuts_passed.push(this_event_counter);

						this_event_counter++;
						
						// measure time for the thread balancing
						thread_scheduler->timeStopProcess(this_thread_id);
					}
				}
			}
		} else {
			thread_scheduler->processerWait();
		}
	}

	// unblocks all threads
	if (num_active_process_threads < num_process_threads)
		thread_scheduler->unlockProcessers();

	passed.lock();
	for (long unsigned i = 0; i < number_events; i++) {
		if (i < number_of_cuts) {
			passed_each_cut[i] += this_thread_passed_each_cut[i];
		}
	}

	// cuts_passed.insert(cuts_passed.end(), this_thread_cuts_passed.begin(), this_thread_cuts_passed.end());

	// for (long unsigned i = 0; i < number_events; i++) {
	// 	cuts_passed[i] = this_thread_cuts_passed[i];
	// }
	passed.unlock();


	// if (this_thread_id == 0)
	// 	myfile.close();
}


void DataAnalysis::loop (unsigned td) {
	unsigned this_thread_id = td;
	

	#ifdef D_AFFINITY
	cpu_set_t cpuset;
	CPU_ZERO( & cpuset);
	CPU_SET( td, & cpuset);

	int erro(::pthread_setaffinity_np( ::pthread_self(), sizeof( cpuset), & cpuset));
	#endif

	passed3.lock();
	string threadId = boost::lexical_cast<std::string>(boost::this_thread::get_id());
	thread_ids.insert(pair<string, unsigned>(threadId, this_thread_id));
	passed3.unlock();

	long _number_events = number_events;
	unsigned this_event_counter;
	unsigned this_event_counter_old = SCHEDULER_UPDATE - 1;
	chrono::time_point<chrono::high_resolution_clock> init;

	bool pass = true;
	unsigned prop_id = 0;


	while (event_counter < _number_events) {
		if (this_thread_id < num_active_process_threads) {
			this_event_counter = event_counter;

			// checks if there are events to process in the file
			if (((this_event_counter+100) >= current_number_events) && !finished_reading) {
				thread_blocked = true;

				boost::this_thread::sleep_for( boost::chrono::microseconds(30) );//barriers
			} else {



				// Wait for proposition table update
				// if (prop_updating) {
				// 	boost::this_thread::sleep_for( boost::chrono::microseconds(50) );//barriers
				// } else {
					// after 100 events updates the cuts
				 if ((this_event_counter % SCHEDULER_UPDATE) < num_process_threads) {
				// cout << " ";
					get_prop2.lock();
					 // cout << this_thread_id << " CENAS " << this_event_counter << ": " << this_event_counter_old << " " << last_sched_update << endl;
					if ((this_event_counter >= this_event_counter_old) && ((this_event_counter % SCHEDULER_UPDATE) == 0) && (this_event_counter > last_sched_update)) {
						
							prop_updating = true;

							last_sched_update = this_event_counter;
							this_event_counter_old = last_sched_update;

							// #ifdef D_HEPF_INTEL
							// cout << "UPDATING " << this_thread_id << ": " << this_event_counter << endl;
							// #endif

							sched->updatePropsWeight();
							// cout << "UPDATING " << this_thread_id << ": " << this_event_counter << endl;

							prop_updating = false;

							// if (current_number_events < _number_events) {
							// 	if (num_process_threads > 2) {
							// 		boost::this_thread::sleep_for( boost::chrono::microseconds(10) );//barriers
							// 	}
							// } else {
							// 	boost::this_thread::sleep_for( boost::chrono::microseconds(10) );//barriers
							// }
					}
					get_prop2.unlock();
				 }

				// measure time for the thread balancing
				thread_scheduler->timeStartProcess(this_thread_id);

				get_prop.lock();
				prop_id = sched->getProposition(this_event_counter);
				 //cout << this_thread_id << "\t" << this_event_counter << "\t"<< prop_id << endl;
				get_prop.unlock();

				if (prop_id != NO_PROPOSITION && prop_id != NEXT_EVENT) {

					Cut c = cuts[prop_id+1];

					init = sched->propositionStart();
					pass = c(this_event_counter);
					// hidden lock in here
					sched->propositionStop(init, prop_id);


					fill_histograms[prop_id].lock();
					fillHistograms(cut_table_inverted[prop_id + 1], this_event_counter);
					fill_histograms[prop_id].unlock();


					if (pass) {
						passed.lock();
						bool ps = sched->passed(this_event_counter);
						if (ps) {
							cuts_passed.push_back(this_event_counter);
						} else {
							if (this_event_counter == event_counter) {
								event_counter++;
							}
						}
						passed.unlock();

						sched->passProposition(prop_id, this_event_counter);

						//if(sched->passedAllProps(this_event_counter)) {
						//	this_thread_cuts_passed[this_thread_id].push_back(this_event_counter);
						//}
					} else {
						failed.lock();
						sched->failProposition(prop_id, this_event_counter);

						if (this_event_counter == event_counter) {
							event_counter++;
						}
						failed.unlock();
					}

					// increments the event counter in case that it is tha last prop
					// hopefully only enters 1 thread here for each event
					// if (cuts_passed[this_event_counter] == number_of_cuts) {
					// 	recordVariablesBoost(this_event_counter);

					// 	if (this_event_counter == event_counter)
					// 		event_counter++;
					// }

					#ifndef D_HEPF_INTEL
					{
						// boost::unique_lock<boost::mutex> _lock (wait_for_queue_pass_mt);
						// wait_for_queue_pass.notify_all();
					}
					#endif
				} else {
					if (prop_id == NEXT_EVENT)
						if (this_event_counter == event_counter)
							event_counter++;

					if (prop_id == NO_PROPOSITION) {
						// #ifndef D_HEPF_INTEL
						// boost::unique_lock<boost::mutex> _lock (wait_for_queue_pass_mt);
						// wait_for_queue_pass.wait(_lock);
						// #else
						// cout << this_thread_id << "\t" << this_event_counter << " " << prop_id << endl;
						boost::this_thread::sleep_for( boost::chrono::microseconds(30) );//barriers
						// #endif
					}
				}

				// measure time for the thread balancing
				thread_scheduler->timeStopProcess(this_thread_id);
			}
		} else {
			thread_scheduler->processerWait();
		}
	}

	// unblocks all threads
	if (num_active_process_threads < num_process_threads)
		thread_scheduler->unlockProcessers();
}

void DataAnalysis::loopSeq (void) {

	thread_id = 0;
	num_process_threads = 1;


	for (unsigned ev = 0; ev < number_events; ev++) {
		event_counter = ev;

		processCuts();

	}

	event_counter++;
}

void DataAnalysis::loadEventsStatic (unsigned td) {

	// solucao javarda para o streamer info???
	if (td > 0)
		boost::this_thread::sleep_for( boost::chrono::microseconds(td*30) );

	DataReader *rd;

	while (!root_reader.empty()) {
		root_reader.pop(rd);
		rd->loadEvents();

		delete rd;
	}

	finished_reading = true;
}

void DataAnalysis::loadEvents (unsigned td) {

	// solucao javarda para o streamer info???
	//if (td > 0)
	//	boost::this_thread::sleep_for( boost::chrono::microseconds(td*30) );

	DataReader *rd;

	while (!root_reader.empty()) {
		if (td < num_active_reader_threads) {
			thread_scheduler->timeStartRead(td);

			root_reader.pop(rd);
			rd->loadEvents();

			root_read.push(rd);

	//		delete rd;

			thread_scheduler->timeStopRead(td);

			// only the master reader updates the table...
			if (td == 0) {
				#ifdef D_THREAD_BALANCE
				thread_scheduler->updateActiveThreads();
				#endif
				
			//	if (!clone_tree) {
			//		to_write = rd->getfChain()->CloneTree(0);
			//		clone_tree = true;
			//	}
			}
		} else {
			thread_scheduler->readerWait();
		}
	}

	finished_reading = true;

	// unblocks all threads
	if (num_active_reader_threads < num_reader_threads)
		thread_scheduler->unlockReaders();
}

void DataAnalysis::produceKNC (unsigned tid) {
	#ifdef D_KNC
		rnd.MKLArrayProducerKNC(tid);
	#endif
}

void DataAnalysis::produce (void) {
	
		rnd.MKLArrayProducer();

}

void DataAnalysis::runSeq (void) {
	t.start();

	int excess_files 	= root_file_list.size() % number_of_processes;
	float excess_factor = (float) excess_files / (float) number_of_processes;
	int files		 	= (float) root_file_list.size() / (float) number_of_processes - excess_factor;

	number_events = 0;

	if (process_id < excess_files)
		files++;

	//DataReader *root_reader;
	root_reader.reserve(files);

	// reads a specific amount of files allocated for each process
	// TODO: each thread should do its own setup to reduce initial single-threaded overhead
	DataReader *rd;

	for (int i = 0; i < files; i++) {
		if (tree_name == "unset") {
			rd = new DataReader (root_file_list[process_id * files + i]);
			rd->setStartEvents(number_events);
			number_events += rd->numberEvents();

			root_reader.push(rd);
		} else {
			rd = new DataReader (root_file_list[process_id * files + i], tree_name);
			rd->setStartEvents(number_events);
			number_events += rd->numberEvents();

			root_reader.push(rd);
		}
	}




	events = new HEPEvent[number_events];

	passed_each_cut = new unsigned[number_of_cuts];
	memset(passed_each_cut, 0, number_of_cuts*sizeof(unsigned));


	initialize();

	event_counter = 0;

	rnd.init(1);

	//********************************************************************
	loadEventsStatic(0);

	loopSeq();

	finalize();


	writeVariables();

	if (save_events)
		writeEvents();


	#ifdef D_MPI
		MPI_Barrier(MPI_COMM_WORLD);
	#endif

	t.stop();

	#ifdef D_REPORT
		reportSeq();
	#endif

	t.report(Report::Verbose);
}

void DataAnalysis::gpuPrngProducer (unsigned tid) {
	rnd.GPUArrayProducer2(tid);
}

void DataAnalysis::run (void) {
	// hostname = knl;
	#ifdef D_SCHEDULER
		runByDefines();
	#else
		runByName();
	#endif
}

void DataAnalysis::runByDefines (void) {
	t.start();


	int excess_files 	= root_file_list.size() % number_of_processes;
	float excess_factor = (float) excess_files / (float) number_of_processes;
	int files		 	= (float) root_file_list.size() / (float) number_of_processes - excess_factor;

	number_events = 0;

	if (process_id < excess_files)
		files++;

	//DataReader *root_reader;
	root_reader.reserve(files);

	// reads a specific amount of files allocated for each process
	// TODO: each thread should do its own setup to reduce initial single-threaded overhead
	DataReader *rd;

	for (int i = 0; i < files; i++) {
		if (tree_name == "unset") {
			rd = new DataReader (root_file_list[process_id * files + i]);
			rd->setStartEvents(number_events);
			number_events += rd->numberEvents();

			// cout << "Process " << process_id << ": " << root_file_list[process_id * files + i] << endl;

			root_reader.push(rd);
		} else {
			rd = new DataReader (root_file_list[process_id * files + i], tree_name);
			rd->setStartEvents(number_events);
			number_events += rd->numberEvents();

			// cout << "Process " << process_id << ": " << root_file_list[process_id * files + i] << endl;

			root_reader.push(rd);
		}
	}

	total_number_events = number_events;

	// saves the average size of the files to update the active threads
	thread_scheduler->setFileSize(number_events/files);

	events = new HEPEvent[number_events];

	passed_each_cut = new unsigned[number_of_cuts];
	memset(passed_each_cut, 0, number_of_cuts*sizeof(unsigned));
	// cuts_passed = new unsigned[number_events];
	// memset(cuts_passed, 0, number_events*sizeof(unsigned));

	sched->initialize(number_events);

	initialize();

	// inits que antes estavam dentro do loop
	#ifdef D_SCHEDULER
	// only inits for las save in case of scheduler
	initRecord(1);
	#else
	initRecord(number_of_cuts);
	#endif
	event_counter = 0;

	num_process_threads = sched->getNumThreads();
	#ifdef D_THREAD_BALANCE
	num_reader_threads = num_process_threads;
	#else
	num_reader_threads = 1;
	#endif

	//********************************************************************

	if (num_reader_threads > files)
		num_reader_threads = files;

	// initial configuration
	thread_scheduler->initialize(num_process_threads, num_reader_threads);

	// starts creating prns
	rnd.init(num_process_threads);

	rnd.initialize(0.0, 0.02);

	#ifdef D_GPU
	boost::thread gpu_producer_threads [num_process_threads];
	for (unsigned tt = 0; tt < num_process_threads; tt++) {
		gpu_producer_threads[tt] = boost::thread(boost::bind(&DataAnalysis::gpuPrngProducer, this, tt));
	}
	#else
	#ifdef D_KNC
	boost::thread gpu_producer_threads [num_process_threads];
	for (unsigned tt = 0; tt < num_process_threads; tt++) {
		gpu_producer_threads[tt] = boost::thread(boost::bind(&DataAnalysis::produceKNC, this, tt));
	}
	#else
	boost::thread generate (boost::bind(&DataAnalysis::produce, this));
	#endif
	#endif


	// holds the starting file for each thread
	nfiles = (unsigned*) malloc (num_reader_threads * sizeof(unsigned));
	int _excess_files 	 = files % num_reader_threads;
	float _excess_factor = (float) _excess_files / (float) num_reader_threads;
	int file_step = ((float) files / (float) num_reader_threads) - _excess_factor + 0.5;

	nfiles[0] = process_id * files;

	for (unsigned td = 0; td < num_reader_threads - 1; td++) {
		nfiles[td+1] = nfiles[td] + file_step;

		if (td < _excess_files)
			nfiles[td+1]++;
	}



	this_thread_cuts_passed = new vector<unsigned>[num_process_threads];

	// DEBUG STUFF
	//num_active_process_threads = num_process_threads;

	if (process_id == 0 )
		thread_scheduler->report();
	// sched->reportTable();


	#ifdef D_VERBOSE
	cout << "Loading events" << endl << endl;
	progressBar(0.0);
	#endif

	Timer io;
	Timer compute;
	float io_time, compute_time;

	io.start();

	// read files
	master_thread = new boost::thread[num_reader_threads];
	for (unsigned tt = 0; tt < num_reader_threads; tt++) {
		master_thread[tt] = boost::thread(boost::bind(&DataAnalysis::loadEvents, this, tt));
	}
	#ifndef D_THREAD_BALANCE
	for (unsigned tt = 0; tt < num_reader_threads; tt++) {
		master_thread[tt].join();
	}

	io_time = (float)io.stop()/1000000.0;
	#endif

	compute.start();

	// processes propositions
	worker_thread = new boost::thread[num_process_threads];
	for (unsigned tt = 0; tt < num_process_threads; tt++) {
		#ifdef D_SCHEDULER
		worker_thread[tt] = boost::thread(boost::bind(&DataAnalysis::loop, this, tt));
		#else
		worker_thread[tt] = boost::thread(boost::bind(&DataAnalysis::loopRegularScheduler, this, tt));
		#endif
	}
	
	#ifdef D_THREAD_BALANCE
	for (unsigned tt = 0; tt < num_reader_threads; tt++) {
		master_thread[tt].join();
	}

	io_time = (float)io.stop()/1000000.0;
	#endif

	for (unsigned tt = 0; tt < num_process_threads; tt++)
		worker_thread[tt].join();

	compute_time = (float)compute.stop()/1000000.0;



	for (long unsigned i = 0; i < num_process_threads; i++) {
		cuts_passed.insert(cuts_passed.end(), this_thread_cuts_passed[i].begin(), this_thread_cuts_passed[i].end());
	}
	

	event_counter++;

	#ifdef D_GPU
	rnd.reportGPU();
	rnd.shutdownGPU();

	for (unsigned tt = 0; tt < num_process_threads; tt++) {
		gpu_producer_threads[tt].join();
	}
	#else
	#ifdef D_KNC
	rnd.shutdownMKL();
	for (unsigned tt = 0; tt < num_process_threads; tt++) {
		gpu_producer_threads[tt].join();
	}
	#else
	rnd.shutdownMKL();
	generate.join();
	#endif
	#endif



	finalize();

	#ifdef D_SCHEDULER
	unsigned a = number_of_cuts;
	number_of_cuts = 1;

	writeVariables();
	//writePdfs();
	number_of_cuts = a;
	#else
	writeVariables();
	//writePdfs();
	#endif

	if (save_events) {
		output.start();
		writeResults();
		writeEvents();
		output_time = (float)output.stop()/1000000.0;
	}


	#ifdef D_MPI
		MPI_Barrier(MPI_COMM_WORLD);
	#endif

	t.stop();

	if (process_id == 0 ) {
		#ifdef D_SCHEDULER
			report();
			sched->reportTable();
		#else
			reportSeq();
		#endif

	}
	t.report(Report::Verbose);

	cout << endl << " => The analysis spent " << io_time << " secs on I/O and " << compute_time << " secs on computation" << endl;
	
	passed3.lock();
	// parece nao dar double free assim...
	//if (root_reader != NULL) delete[] root_reader;
	// if (rnd != NULL) 		 delete[] rnd;		   rnd = NULL;	//*****
	if (events != NULL) 	 delete[] events;	   events = NULL;//*****
	// if (cuts_passed != NULL) delete[] cuts_passed; cuts_passed = NULL;
	if (nfiles != NULL)		 delete[] nfiles;	   nfiles = NULL;
	if (sched != NULL)		 delete sched;		   sched = NULL;
	if (thread_scheduler != NULL) delete thread_scheduler; thread_scheduler = NULL;

	if (master_thread != NULL)  delete[] master_thread; master_thread = NULL;
	if (worker_thread != NULL)  delete[] worker_thread; worker_thread = NULL;
	passed3.unlock();
}

void DataAnalysis::runByName (void) {
	t.start();


	int excess_files 	= root_file_list.size() % number_of_processes;
	float excess_factor = (float) excess_files / (float) number_of_processes;
	int files		 	= (float) root_file_list.size() / (float) number_of_processes - excess_factor;

	number_events = 0;

	if (process_id < excess_files)
		files++;

	//DataReader *root_reader;
	root_reader.reserve(files);
	root_read.reserve(files);

	// reads a specific amount of files allocated for each process
	// TODO: each thread should do its own setup to reduce initial single-threaded overhead
	DataReader *rd;

	for (int i = 0; i < files; i++) {
		if (tree_name == "unset") {
			rd = new DataReader (root_file_list[process_id * files + i]);
			rd->setStartEvents(number_events);
			number_events += rd->numberEvents();

			// cout << "Process " << process_id << ": " << root_file_list[process_id * files + i] << endl;

			root_reader.push(rd);
		} else {
			rd = new DataReader (root_file_list[process_id * files + i], tree_name);
			rd->setStartEvents(number_events);
			number_events += rd->numberEvents();

			// cout << "Process " << process_id << ": " << root_file_list[process_id * files + i] << endl;

			root_reader.push(rd);
		}
	}

	total_number_events = number_events;

	// saves the average size of the files to update the active threads
	thread_scheduler->setFileSize(number_events/files);

	events = new HEPEvent[number_events];

	passed_each_cut = new unsigned[number_of_cuts];
	memset(passed_each_cut, 0, number_of_cuts*sizeof(unsigned));
	// cuts_passed = new unsigned[number_events];
	// memset(cuts_passed, 0, number_events*sizeof(unsigned));

	sched->initialize(number_events);

	initialize();

	// inits que antes estavam dentro do loop
	if (!strcmp(hostname.c_str(), knl.c_str())) {
		// only inits for las save in case of scheduler
		initRecord(1);
	} else {
		initRecord(number_of_cuts);
	}
	event_counter = 0;

	num_process_threads = sched->getNumThreads();
	#ifdef D_THREAD_BALANCE
	num_reader_threads = num_process_threads;
	#else
	num_reader_threads = 1;
	#endif

	//********************************************************************

	if (num_reader_threads > files)
		num_reader_threads = files;

	// initial configuration
	// TODO: devia estar no inicio da main....
	thread_scheduler->initialize(num_process_threads, num_reader_threads);
	// starts creating prns
	rnd.init(num_process_threads);

	#ifdef D_GPU
	boost::thread gpu_producer_threads [num_process_threads];
	for (unsigned tt = 0; tt < num_process_threads; tt++) {
		gpu_producer_threads[tt] = boost::thread(boost::bind(&DataAnalysis::gpuPrngProducer, this, tt));
	}
	#else
	#ifdef D_KNC
	boost::thread gpu_producer_threads [num_process_threads];
	for (unsigned tt = 0; tt < num_process_threads; tt++) {
		gpu_producer_threads[tt] = boost::thread(boost::bind(&DataAnalysis::produceKNC, this, tt));
	}
	#else
	boost::thread generate (boost::bind(&DataAnalysis::produce, this));
	#endif
	#endif

	// holds the starting file for each thread
	nfiles = (unsigned*) malloc (num_reader_threads * sizeof(unsigned));
	int _excess_files 	 = files % num_reader_threads;
	float _excess_factor = (float) _excess_files / (float) num_reader_threads;
	int file_step = ((float) files / (float) num_reader_threads) - _excess_factor + 0.5;

	nfiles[0] = process_id * files;

	for (unsigned td = 0; td < num_reader_threads - 1; td++) {
		nfiles[td+1] = nfiles[td] + file_step;

		if (td < _excess_files)
			nfiles[td+1]++;
	}

	this_thread_cuts_passed = new vector<unsigned>[num_process_threads];

	// DEBUG STUFF
	//num_active_process_threads = num_process_threads;

	if (process_id == 0 )
		thread_scheduler->report();
	// sched->reportTable();


	#ifdef D_VERBOSE
	cout << "Loading events" << endl << endl;
	progressBar(0.0);
	#endif

	io.start();

	// read files
	master_thread = new boost::thread[num_reader_threads];
	for (unsigned tt = 0; tt < num_reader_threads; tt++) {
		master_thread[tt] = boost::thread(boost::bind(&DataAnalysis::loadEvents, this, tt));
	}
	
	#ifndef D_THREAD_BALANCE
	for (unsigned tt = 0; tt < num_reader_threads; tt++) {
		master_thread[tt].join();
	}

	io_time = (float)io.stop()/1000000.0;
	#endif


	compute.start();

	// processes propositions
	worker_thread = new boost::thread[num_process_threads];

	for (unsigned tt = 0; tt < num_process_threads; tt++) {
		worker_thread[tt] = boost::thread(boost::bind(&DataAnalysis::loopRegularScheduler, this, tt));
	}
	
	#ifdef D_THREAD_BALANCE
	for (unsigned tt = 0; tt < num_reader_threads; tt++) {
		master_thread[tt].join();
	}

	io_time = (float)io.stop()/1000000.0;
	#endif

	for (unsigned tt = 0; tt < num_process_threads; tt++) {
		worker_thread[tt].join();
	}

	compute_time = (float)compute.stop()/1000000.0;
	

	for (long unsigned i = 0; i < num_process_threads; i++) {
		cuts_passed.insert(cuts_passed.end(), this_thread_cuts_passed[i].begin(), this_thread_cuts_passed[i].end());
	}
	
	sort(cuts_passed.begin(), cuts_passed.end());


	#ifdef D_GPU
	rnd.reportGPU();
	rnd.shutdownGPU();

	for (unsigned tt = 0; tt < num_process_threads; tt++) {
		gpu_producer_threads[tt].join();
	}
	#else
	rnd.shutdownMKL();
	#ifdef D_KNC
	for (unsigned tt = 0; tt < num_process_threads; tt++) {
		rnd.shutdownKNC(tt);
		gpu_producer_threads[tt].join();
	}
	// for (unsigned tt = 0; tt < num_process_threads; tt++) {
	// 	gpu_producer_threads[tt].join();
	// }
	// cout << "cena3"<< endl;
	#else
	generate.join();
	#endif
	#endif

	event_counter++;
	rnd.shutdownMKL();


	finalize();


	if (!strcmp(hostname.c_str(), knl.c_str())) {
		unsigned a = number_of_cuts;
		number_of_cuts = 1;

		writeVariables();
		//writePdfs();
		number_of_cuts = a;
	} else {
		writeVariables();
		//writePdfs();
	}

	if (save_events) {
		output.start();
		writeResults();
		writeEvents();
		output_time = (float)output.stop()/1000000.0;
	}

	#ifdef D_MPI
		MPI_Barrier(MPI_COMM_WORLD);
	#endif

	t.stop();

	if (process_id == 0 ) {
		// if (strcmp(hostname.c_str(), knl.c_str())) {
		// 	report();
		// 	sched->reportTable();
		// 	cout << "aqui"<< endl;
		// } else {
			reportSeq();
		// }
	}

	t.report(Report::Verbose);

	if (save_events)
		cout << endl << " => The analysis spent " << io_time << " secs on input reading, " << compute_time << " secs on computation and " << output_time << " secs on event storage" << endl;
	else
		cout << endl << " => The analysis spent " << io_time << " secs on input reading and " << compute_time << " secs on computation" << endl;

	passed3.lock();
	// parece nao dar double free assim...
	//if (root_reader != NULL) delete[] root_reader;
	// if (rnd != NULL) 		 delete[] rnd;		   rnd = NULL;	//*****
	if (events != NULL) 	 delete[] events;	   events = NULL;//*****
	// if (cuts_passed != NULL) delete[] cuts_passed; cuts_passed = NULL;
	if (nfiles != NULL)		 delete[] nfiles;	   nfiles = NULL;
	if (sched != NULL)		 delete sched;		   sched = NULL;
	if (thread_scheduler != NULL) delete thread_scheduler; thread_scheduler = NULL;

	if (master_thread != NULL)  delete[] master_thread; master_thread = NULL;
	if (worker_thread != NULL)  delete[] worker_thread; worker_thread = NULL;
	passed3.unlock();
}

//inline
void DataAnalysis::processCuts (void) {
	bool pass = true;
	unsigned cut_number = 0;
	unsigned cut_id;
	chrono::time_point<chrono::high_resolution_clock> init;
	Cut c;

	// ofstream myfile;
	// myfile.open ("events_sequential.txt");

	for (unsigned i = 1; i <= number_of_cuts && pass; i++) {
		c = cuts[i];

		pass = c(event_counter);

		if (pass) {
			passed_each_cut[i-1]++;
			recordVariables(i-1, event_counter);

			// if (i == number_of_cuts)
			// 	myfile << event_counter << endl;
		}

		cut_number++;
	}
	// myfile.close();
}

//inline
void DataAnalysis::processCutsNoRecord (void) {
	bool pass = true;
	unsigned cut_number = 0;
	unsigned cut_id;
	chrono::time_point<chrono::high_resolution_clock> init;
	Cut c;

	for (unsigned i = 1; i <= number_of_cuts && pass; i++) {
		cut_id = i;
		c = cuts[cut_id];

		init = sched->propositionStart();

		pass = c(event_counter);

		sched->propositionStop(init, cut_id);

		if (pass) {

			#ifdef D_REPORT
			#endif
		} else {
			#ifdef D_REPORT
			#endif
		}
		cut_number++;
	}
}

void DataAnalysis::initialize (void) {
}

void DataAnalysis::finalize (void) {
}

void DataAnalysis::initRecord (unsigned cuts) {
}

void DataAnalysis::reportSeq (void) {

	cout << number_events << " events analysed" << endl << endl;

	cout << "Number of events that passed each cut: " << endl;

	for (unsigned i = 0; i < number_of_cuts; i++)
		cout << "Cut " << i+1 << ": " << passed_each_cut[i] << endl;
	//printCuts();
}

void DataAnalysis::report (void) {

	#ifdef D_MPI
	long unsigned local_events = number_events;
	long unsigned global_events;

	MPI_Reduce(&local_events, &global_events, 1, MPI_UNSIGNED_LONG, MPI_SUM, 0, MPI_COMM_WORLD);

	if (process_id == 0)
		cout << global_events << " events analysed by " << number_of_processes << " processes with " << num_process_threads << " threads each" << endl << endl;
	#else
	cout << number_events << " events analysed by process " << process_id << " with " << num_process_threads << " threads" << endl << endl;
	#endif

	if (process_id == 0)
	cout << "Number of events that passed each cut: " << endl;

	if (process_id == 0)
	sched->reportDebug();
	//printCuts();
}

unsigned DataAnalysis::addCut (string name, Cut c) {
	cuts.insert(make_pair(cut_counter, c));
	cut_table.insert(make_pair(name, cut_counter));
	cut_table_inverted.insert(make_pair(cut_counter, name));

	cut_counter++;

	return cuts.size();
}

/*
void DataAnalysis::printCuts (void) {
	for (unsigned i = 0; i < number_of_cuts + 1; i++) {
		if (!i) {
			cout << "\tstart\t";

			for (auto &ct : cut_table)
				cout << ct.first << "\t";

			cout << endl;
		}

		for (unsigned j = 0; j < number_of_cuts + 1; j++) {
			if (!j) {
				if(!i)
					cout << "start\t";

				unsigned b = 0;
				for (auto &ct : cut_table) {
					if (b == (i - 1)) {
						cout << ct.first << "\t";
						break;
					}

					b++;
				}
			}

			cout << cut_organiser.getCutDependency(i, j) << "\t";
		}
		cout << endl;
	}
	cout << endl << endl;

	printCutsOrder();
}

void DataAnalysis::printCutsOrder (void) {

	cout << "== Cut order ==" << endl;
	for (unsigned i = 1; i < number_of_cuts + 1; i++)
		for (auto &a : cut_table)
			if (a.second == cut_organiser.getCutOrder(i))
				cout << "\t" << a.first << endl;

	cout << endl;
}
*/

bool DataAnalysis::addCutDependency (string cut1, string cut2) {
	// check if the cut exists on the lookup table
	if (cut_table.find(cut1) == cut_table.end() || cut_table.find(cut2) == cut_table.end() )
		return false;

	// checks if it's a duplicate
	if (cut1 == cut2)
		return false;

	// add the dependency
	sched->setPropositionDep(cut_table.find(cut1)->second, cut_table.find(cut2)->second);

	return true;
}

void DataAnalysis::progressBar (float progress) {
	// float progress = 0.0;
	// while (progress < 1.0) {
	int barWidth = 70;
	
	// cout << '\r' << std::flush;
	std::cout << "[";
	int pos = barWidth * progress;
	for (int i = 0; i < barWidth; ++i) {
		if (i < pos) std::cout << "=";
		else if (i == pos) std::cout << ">";
		else std::cout << " ";
	}
	std::cout << "] " << int(progress * 100.0) << " %\r";
	std::cout.flush();

	// 	progress += 0.16; // for demonstration only
	// }
}
void DataAnalysis::writeResults (void) {
}

void DataAnalysis::writeEvents (void) {
	// #ifdef D_VERBOSE
	// cout << "Writing events" << endl << endl;
	// long unsigned last_n = 0;
	// progressBar(0.0);
	// #endif

	// DataReader *rd;
	// root_read.pop(rd);

	// long unsigned number_events_cuts_passed = cuts_passed.size();

	// TFile *out_file = new TFile (output_root_file.c_str(), "recreate");

	// TTree *new_to_write = rd->getfChain()->CloneTree(0);


	// for (long unsigned i = 0; i < number_events_cuts_passed; i++) {
	// 	#ifdef D_VERBOSE
	// 	if (i == (number_events_cuts_passed - 1)) {
	// 		progressBar(1);
	// 		cout << endl << endl << "All events wrote" << endl << endl;
	// 	} else if (i >= last_n + 2000) {
	// 		float n = (float)i/(float)number_events_cuts_passed;
	// 		last_n = i;
	// 		progressBar(n);
	// 	}
	// 	#endif

	// 	events[cuts_passed[i]].loadEvent(cuts_passed[i]);
	// 	events[cuts_passed[i]].write(new_to_write);
	// }

	// out_file->Write();
	// out_file->Close();

	

	DataReader *rd;
	root_read.pop(rd);

	rd->writeEvents(cuts_passed, output_root_file);


}

void DataAnalysis::recordVariables (unsigned cut_number, long unsigned this_event_counter) {
}

void DataAnalysis::recordVariablesBoost (long unsigned event) {
}

void DataAnalysis::writeVariables (void) {
}

// deprecated
/*void DataAnalysis::readPdf (vector<string> signals, vector<string> backgrounds) {
	// Read signal pdf
	if (signal_file != "unset") {
		TFile file (signal_file.c_str());

		for (unsigned i = 0; i < signals.size(); i++) {
			TH1D *p = (TH1D*) file.Get(signals[i].c_str());
			signal_pdf.push_back(*p);
		}
	}

	// Read background pdf
	if (background_file != "unset") {
		TFile file (background_file.c_str());

		for (unsigned i = 0; i < backgrounds.size(); i++) {
			TH1D *p = (TH1D*) file.Get(backgrounds[i].c_str());
			background_pdf.push_back(*p);
		}
	}

	pdfs = true;
}*/

void DataAnalysis::fillHistograms (string cut_name, long unsigned this_event_counter) {

}

void DataAnalysis::writeAllHistograms (void) {
}

long unsigned DataAnalysis::currentEvent (void) {
	return event_counter;
}
