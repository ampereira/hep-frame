#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;

class AnalysisGenerator {
	string name;
	string dir;
	string tree_name;

public:
	AnalysisGenerator (string _name, string _dir, string _tree_name);
	void generate (void);
};
