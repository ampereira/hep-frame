#include "AnalysisGenerator.h"

AnalysisGenerator::AnalysisGenerator (string _name, string _dir, string _tree_name) {
	name = _name;
	dir = _dir;
	tree_name = _tree_name;
}

void AnalysisGenerator::generate (void) {
	ofstream out_header (dir + name + ".h");
	ofstream out_body (dir + name + "_cfg.cxx");
	ofstream out_body_user (dir + name + ".cxx");

	out_header << "#ifndef " << name << "_h" << endl;
	out_header << "#define " << name << "_h" << endl << endl;
	out_header << "#include <cstdlib>" << endl;
	out_header << "#include <iostream>" << endl;
	out_header << "#include <string>" << endl;
	out_header << "#include <vector>" << endl;
	out_header << "#include <algorithm>" << endl;
	// out_header << "#include <omp.h>" << endl;
	out_header << "#include <DataAnalysis.h>" << endl;
	out_header << "#ifdef D_MPI" << endl;
	out_header << "\t#include <mpi.h>" << endl;
	out_header << "#endif" << endl;
	out_header << endl << "using namespace std;" << endl << endl << endl;

	out_header << "class " << name << " : public DataAnalysis {" << endl;
	out_header << "\t// Variables to record per cut" << endl << /*endl << "\t// Pdfs to record" << endl <<*/ endl << endl;
	out_header << "\t// Insert your class variables here" << endl << endl << endl;

	out_header << "\tvoid recordVariables (unsigned cut_number, long unsigned this_event_counter);" << endl;
	out_header << "\tvoid recordVariablesBoost (long unsigned event);" << endl;
	out_header << "\tvoid writeVariables (void);" << endl;
	//out_header << "\tvoid recordPdfs (void);" << endl;
	out_header << "\tvoid initRecord (unsigned cuts);" << endl;
	out_header << "\tvoid writeResults (void);" << endl;
	out_header << "\tvoid fillHistograms (string cut_name, long unsigned this_event_counter);" << endl;
	out_header << "\tvoid initialize (void);" << endl;
	out_header << "\tvoid finalize (void);" << endl << endl;
	out_header << "public:" << endl;
	out_header << "\t" << name << " (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, unsigned ncuts);" << endl;
	out_header << "\t" << name << " (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, string _tree_name, unsigned ncuts);" << endl;
	out_header << "};\n\n#endif" << endl;


	out_body_user << "#include \"" << name << ".h\"" << endl;
	out_body_user << "#include \"EventInterface.h\"" << endl << endl;
	out_body_user << "using namespace std;" << endl << endl;
	out_body_user << "extern HEPEvent *events;" << endl;
	out_body_user << "extern PseudoRandomGenerator rnd;" << endl;
	out_body_user << "extern map<string, unsigned> thread_ids;" << endl << endl;
	out_body_user << "// Automatically access your histograms by th1d[\"dir\"][\"hist_name\"]->Print()..." << endl;
	out_body_user << "extern map<string, map <string, TH1D*>> th1d;" << endl;
	out_body_user << "extern map<string, map <string, TH2D*>> th2d;" << endl;
	out_body_user << "extern map<string, map <string, TH3D*>> th3d;" << endl;
	// out_body_user << "extern TRandom3 *rnd;" << endl;	// TO CHANGE FOR THE REAL PRN
	out_body_user /*<< "#pragma omp threadprivate(event_counter)" << endl*/ << endl << endl;

	out_body_user << "// Use this constructor if you don't specify a ttree in the input file" << endl;
	out_body_user << name << "::" << name << " (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, unsigned ncuts) : DataAnalysis (_root_file, _tree_filename, _output_root_file, _signal, _background, ncuts) {" << endl;
	out_body_user << "\t// Set to true if you want to save events that pass all cuts" << endl;
	out_body_user << "\tif (_output_root_file.empty())" << endl;
	out_body_user << "\t\tsave_events = false;" << endl;
	out_body_user << "\telse" << endl;
	out_body_user << "\t\tsave_events = true;" << endl << endl;
	out_body_user << "\t// Initialise your class variables here" << endl << endl;
	out_body_user << "}" << endl << endl;

	out_body_user << "// Use this constructor if you specify a ttree in the input file" << endl;
	out_body_user << name << "::" << name << " (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, string _tree_name, unsigned ncuts) : DataAnalysis (_root_file, _tree_filename, _output_root_file, _signal, _background, _tree_name, ncuts) {" << endl;
	out_body_user << "\t// Set to true if you want to save events that pass all cuts" << endl;
	out_body_user << "\tif (_output_root_file.empty())" << endl;
	out_body_user << "\t\tsave_events = false;" << endl;
	out_body_user << "\telse" << endl;
	out_body_user << "\t\tsave_events = true;" << endl << endl;
	out_body_user << "\t// Initialise your class variables here" << endl << endl;
	out_body_user << "}" << endl << endl;

	out_body_user << "// Initialize any information before processing the events" << endl;
	out_body_user << "void " << name << "::initialize (void) {" << endl << endl;
	out_body_user << "\t// Set this variable to the cut that you wish to save the events, only 1 cut is allowed" << endl;
	out_body_user << "\t// The first cut starts with index 0; if not set it will save after the last cut by default" << endl;
	out_body_user << "\t// Events will only be saved if the binary is executed with the -o option" << endl;
	out_body_user << "\t// If this option is set without the variable below, only the events that pass all cuts are stored" << endl;
	out_body_user << "\t//cut_to_save_events = 0; // uncoment this if you want to save the events that pass cut 0" << endl;
	out_body_user << "}" << endl << endl;

	out_body_user << "// Use this method to fill any histograms you may want" << endl;
	out_body_user << "// This method is called after each cut" << endl;
	out_body_user << "// You can fill information for a specific cut by comparing the cut_name to the cut's name" << endl;
	out_body_user << "// You should declare the histograms in the " << name << ".h file" << endl;
	out_body_user << "void " << name << "::fillHistograms (string cut_name, long unsigned this_event_counter) {" << endl << endl;
	out_body_user << "\t// Use this as an example to specify a given cut" << endl;
	out_body_user << "\t// if (cut_name == \"cut0\") {"<< endl << "\t\t// fill stuff...."<<endl<<"\t// }" <<endl;
	out_body_user << "}" << endl << endl;

	out_body_user << "// Finalize any calculations after processing all events" << endl;
	out_body_user << "// If you declared and filled histograms you should write them into a file in this method" << endl;
	out_body_user << "void " << name << "::finalize (void) {" << endl << endl;
	out_body_user << "}" << endl << endl;

	out_body_user << "// Sample cut, always define a cut like this" << endl;
	out_body_user << "bool cut (unsigned this_event_counter) {" << endl;
	out_body_user << "\t// A cut should return true or false" << endl;
	out_body_user << "\treturn true;" << endl;
	out_body_user << "}" << endl << endl;


	out_body << "#include \"" << name << ".h\"" << endl << endl;
	out_body << "#define THIS_THREAD(x) thread_id * number_of_cuts + x" << endl << endl;
	out_body << "using namespace std;" << endl << endl;
	out_body << "extern boost::lockfree::queue<DataReader*> root_reader;" << endl;
	out_body << "extern boost::lockfree::queue<DataReader*> root_read;" << endl;
	out_body << "extern HEPEvent *events;" << endl;
	out_body << "extern long unsigned event_counter;" << endl;
	out_body << "extern unsigned num_threads;" << endl;
	out_body << "extern unsigned thread_id;" << endl;

	out_body << "boost::mutex *record_vars;" << endl << endl << endl;

	out_body << "// Write here the variables and expressions to record per cut" << endl;
	out_body << "#ifdef RecordVariables" << endl << endl;
	out_body << "#endif" << endl;
	out_body << "// ***********************************************" << endl << endl << endl;
	
	out_body /*<< "#pragma omp threadprivate(event_counter)" << endl*/ << endl << endl;
	out_body << "// ***********************************************" << endl;
	out_body << "// Method to record variables, do not edit" << endl;
	out_body << endl << endl << endl;

	out_body << "// end" << endl;


	// write the writeResults function
	out_body << "void " << name << "::writeResults (void) {" << endl;
	out_body << "\t#ifdef D_VERBOSE\n\tcout << \"Writing events\" << endl << endl;\n\tlong unsigned last_n = 0;\n\tprogressBar(0.0);\n\t#endif" << endl;
	out_body << "\n\tDataReader *rd;\n\troot_read.pop(rd);\n" << endl;
	out_body << "\tTFile *out_file = new TFile (output_root_file.c_str(), \"recreate\");" << endl;
	out_body << "\tTTree *new_to_write = rd->getfChain()->CloneTree(0);\n" << endl;
	out_body << "\tlong unsigned number_events_cuts_passed = cuts_passed.size();\n" << endl;
	out_body << "\tfor (long unsigned i = 0; i < number_events_cuts_passed; i++) {" << endl;
	out_body << "\t\t#ifdef D_VERBOSE" << endl;
	out_body << "\t\tif (i == (number_events_cuts_passed - 1)) {" << endl;
	out_body << "\t\t\tprogressBar(1);" << endl;
	out_body << "\t\t\tcout << endl << endl << \"All events wrote\" << endl << endl;" << endl;
	out_body << "\t\t} else if (i >= last_n + 2000) {" << endl;
	out_body << "\t\t\tfloat n = (float)i/(float)number_events_cuts_passed;" << endl;
	out_body << "\t\t\tlast_n = i;" << endl;
	out_body << "\t\t\tprogressBar(n);\n\t\t}" << endl;
	out_body << "\t\t#endif\n" << endl;
	out_body << "\t\tevents[cuts_passed[i]].loadEvent(cuts_passed[i]);" << endl;
	out_body << "\t\tevents[cuts_passed[i]].write(new_to_write);\n\t}\n" << endl;
	out_body << "\tout_file->Write();" << endl;
	out_body << "\tout_file->Close();" << endl;
	out_body << "}" << endl;


	/*out_body << "// ***********************************************" << endl;
	out_body << "// Method to record pdfs, do not edit" << endl;
	out_body << endl;


	out_body << "// Write here the variables to record as pdf" << endl;
	out_body << "#ifdef PdfVariables" << endl << endl;
	out_body << "#endif" << endl;
	out_body << "// ***********************************************" << endl << endl << endl;
	*/
	
	out_body_user << "int main (int argc, char *argv[]) {" << endl;
	out_body_user << "\tmap<string, string> options;\n\tProgramOptions opts;" << endl;
	out_body_user << "\tvector<string> file_list;" << endl << endl;
	out_body_user << "\t#ifdef D_MPI" << endl;
	out_body_user << "\t\tMPI_Init(&argc, &argv);" << endl;
	out_body_user << "\t#endif" << endl << endl;
	out_body_user << "\tif (!opts.getOptions(argc, argv, options, file_list))\n\t\treturn 0;" << endl << endl;
	out_body_user << "\t// Do not forget to set the number of cuts to add!!!!!" << endl;
	out_body_user << "\tunsigned number_of_cuts = 1;" << endl << endl;

	if (tree_name == "unset")
		out_body_user << "\t" << name << " anl (file_list, options[\"record\"], options[\"output\"], options[\"signal\"], options[\"background\"], number_of_cuts);" << endl;
	else
		out_body_user << "\t" << name << " anl (file_list, options[\"record\"], options[\"output\"], options[\"signal\"], options[\"background\"], \"" << tree_name << "\", number_of_cuts);" << endl << endl;

	out_body_user << "\t// Add the cuts using the addCut method" << endl;
	out_body_user << "\tanl.addCut(\"cut\", cut);" << endl << endl;
	out_body_user << "\t// Initialize the PRNG with (avg, stddev) for gaussian distributions; if you desire do (avg, stddev, seed)\n\t// Seed should only be set for debugging purposes" << endl;
	out_body_user << "\t// Chose the PRNG using the setGenerator method; note that GPU PRNG will always be used if export GPU=yes is set befor compiling" << endl;
	out_body_user << "\t// If MKL is chosen use the Intel compiler by setting export HEPF_INTEL=yes before compiling" << endl;
	out_body_user << "\trnd.setGenerator(TRandom);" << endl;
	out_body_user << "\trnd.initialize(0.0, 0.02);" << endl << endl;
	out_body_user << "\t// Processes the analysis" << endl;
	out_body_user << "\t#ifdef D_SEQ" << endl;
	out_body_user << "\tanl.runSeq();" << endl;
	out_body_user << "\t#else" << endl;
	out_body_user << "\tanl.run();" << endl;
	out_body_user << "\t#endif" << endl << endl;
	out_body_user << "\t#ifdef D_MPI" << endl;
	out_body_user << "\t\tMPI_Finalize();" << endl;
	out_body_user << "\t#endif" << endl << endl;
	out_body_user << "\treturn 0;\n}" << endl;

	out_header.close();
	out_body.close();
}
