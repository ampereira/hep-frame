#include <cstdlib>
#include <iostream>
#include <string>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <boost/program_options.hpp>
#include "AnalysisGenerator.h"

using namespace std;
namespace po = boost::program_options;

string dir;
string analysis;
string tree_name;


bool programOptions (int argc, char *argv[]) {
	// creates the program options
	po::options_description desc ("Valid options");
	desc.add_options()
		("help,h", "produces the help message")
		("dir,d", po::value<string>(), "directory of the analysis")
		("analysis,a", po::value<string>(), "analysis name")
		("tree,t", po::value<string>(), "tree name")
	;
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	// get the options
	if (vm.count("help")) {
		cout << desc << endl;
		return false;
	}

	if (vm.count("dir")) {
		dir = vm["dir"].as<string>();
		struct stat info;

		if(stat(dir.c_str(), &info) != 0) {
			cerr << "Output directory does not exist" << endl;
			return false;
		}
	} else {
		cerr << "Output directory not set" << endl;
		return false;
	}

	if (vm.count("analysis")) {
		analysis = vm["analysis"].as<string>();
	} else {
		cerr << "Input analysis name not set" << endl;
	}

	if (vm.count("tree")) {
		tree_name = vm["tree"].as<string>();
	} else {
		tree_name = "unset";
	}

	return true;
}


int main (int argc, char *argv[]) {

	if (!programOptions(argc, argv))
		return -1;

	AnalysisGenerator gen (analysis, dir, tree_name);

	gen.generate();

	return 0;
}
