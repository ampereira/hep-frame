#include "RecordVariables.h"

using namespace std;

RecordVariables::RecordVariables (string _filename, string _event_filename) {
	filename = _filename;
	event_filename = _event_filename;

	// add operands
	operands.push_back("+");
	operands.push_back("-");
	operands.push_back("*");
	operands.push_back("/");
	operands.push_back("%");
}

vector<string> RecordVariables::untokenize (vector<string> res) {
	vector<string> result;

	// check if it is a defective vector
	if ((res[0].find("<") != string::npos) && (res[0].find(">") == string::npos)) {
		stringstream ss;
		bool stop = false;
		unsigned i;

		for (i = 0; !stop; i++) {
			if (res[i].find(">") != string::npos) {
				stop = true;
				ss << res[i];
			} else
				ss << res[i] << " ";
		}

		result.push_back(ss.str());
		result.push_back(res[i]);

		return result;
	} else {
		return res;
	}
}

void RecordVariables::parseEventFile (void) {
	ifstream event_file (event_filename);
	string line;
	bool parse = false;

	// loops over the event file
	while (getline(event_file, line)) {
		// check to start or stop parsing
		if (line.find(START_PARSING_EVENT_FILE) != string::npos) {
			parse = true;
			continue;
		} else
			if (parse && line.find(STOP_PARSING_EVENT_FILE) != string::npos)
				parse = false;

		if (parse) {
			stringstream strstr (line);

			istream_iterator<string> it(strstr);
			istream_iterator<string> end;
			vector<string> results(it, end);

			if (!results.empty()) {
				// if its not a comment
				if (results[0].find("//") == string::npos) {
					// refine the quality of the parse
					results = untokenize(results);
					results[1].erase(remove(results[1].begin(), results[1].end(), ';'), results[1].end());

					// check if it is an array, scalar or pointer-vector
					// check if its a pointer to vector
					if ((results[0].find("vector") != string::npos) && (results[0].find("*") != string::npos || results[1].find("*") != string::npos)){
						// removes the pointer declaration
						results[0].erase(remove(results[0].begin(), results[0].end(), '*'), results[0].end());
						results[1].erase(remove(results[1].begin(), results[1].end(), '*'), results[1].end());

						pointer_vectors.push_back(make_pair(results[0], results[1]));
					} else {
						// check if it is an array or regular vector
						if ((results[1][results[1].size()-1] == ']')) {
							// get the size of the array
							string size = results[1].substr(results[1].find("[") + 1);
							size = size.substr(0, results[1].find("]"));

							arrays.push_back(make_tuple(results[0], results[1].substr(0, results[1].find("[")), strtoul(size.c_str(), NULL, 0)));
						}
						else {
							// checks if it is a regular vector
							if (results[0].find("vector") != string::npos) {
								vectors.push_back(make_pair(results[0], results[1]));
							} else {
								// its a scalar!
								scalars.push_back(make_pair(results[0], results[1]));
							}
						}
					}
				}
			}
		}
	}

	event_file.close();
}

bool RecordVariables::checkVariable (string var) {
	bool valid = false;

	// checks if the variables exist on the event file
	for (unsigned j = 0; j < scalars.size() && !valid; ++j) {
		string sc;

		if (var.at(0) == '-')
			sc = var.substr(1);
		else
			sc = var;

		string aux = scalars[j].second;

		// remove the pointer for the comparison
		if (aux[0] == '*')
			aux.erase(0,1);

		if (sc == aux)
			valid = true;
	}

	// check if its an array
	if (!valid)
		valid = checkArray (var);

	if (!valid)
		cerr << "Invalid variable: " << var << ", perhaps you need to specify a position if it's a vector" << endl;

	return valid;
}

bool RecordVariables::checkPointerVector (string var) {
	bool valid = false;
	size_t pos;

	if ((pos = var.find('[')) != string::npos || (pos = var.find("->at(")) != string::npos || (pos = var.find(".at(")) != string::npos) {
		// when a position is specified
		unsigned i;
		string nvar = var.substr(0, pos);
		string p;

		for (i = 0; i < pointer_vectors.size() && !valid; i++) {
			if (nvar == pointer_vectors[i].second)
				valid = true;
		}

		if (valid) {
			if (var[pos+1] >= '0' && var[pos+1] <= '9')	// case of []
				pos++;
			else
				if (var[pos+4] >= '0' && var[pos+4] <= '9')	// case of .at()
					pos+=4;
				else
					if (var[pos+5] >= '0' && var[pos+5] <= '9')	// case of ->at()
						pos+=5;

			stringstream ss;
			ss<<nvar<<"_"<<var[pos];

			string type = pointer_vectors[i-1].first;

			type.erase(type.find("vector"),6);
			type.erase(type.find("<"),1);
			type.erase(type.find(">"),1);

			// avoid duplicates
			if (!isDuplicatePointerVector(ss.str())) {
				pointer_vectors_to_write.push_back(make_pair(type, ss.str()));
			}
		}
	} else {
		// when no position is specified
		valid = false;
	}
	return valid;
}

bool RecordVariables::checkVector (string var) {
	bool valid = false;
	size_t pos;

	if ((pos = var.find('[')) != string::npos || (pos = var.find("->at(")) != string::npos || (pos = var.find(".at(")) != string::npos) {
		// when a position is specified
		unsigned i;
		string nvar = var.substr(0, pos);
		string p;

		for (i = 0; i < vectors.size() && !valid; i++) {
			if (nvar == vectors[i].second)
				valid = true;
		}

		if (valid) {
			if (var[pos+1] >= '0' && var[pos+1] <= '9')	// case of []
				pos++;
			else
				if (var[pos+4] >= '0' && var[pos+4] <= '9')	// case of .at()
					pos+=4;
				else
					if (var[pos+5] >= '0' && var[pos+5] <= '9')	// case of ->at()
						pos+=5;

			stringstream ss;
			ss<<nvar<<"_"<<var[pos];

			string type = vectors[i-1].first;

			type.erase(type.find("vector"),6);
			type.erase(type.find("<"),1);
			type.erase(type.find(">"),1);

			// avoid duplicates
			if (!isDuplicateVector(ss.str())) {
				vectors_to_write.push_back(make_pair(type, ss.str()));
			}
		}
	} else {
		// when no position is specified
		valid = false;
	}
	return valid;
}

tuple <string, string, unsigned> RecordVariables::findArray (string name) {

	for (unsigned it = 0; it < arrays.size(); ++it) {
		if (get<1>(arrays[it]) == name)
			return make_tuple(get<0>(arrays[it]), get<1>(arrays[it]), get<2>(arrays[it]));
	}

	return make_tuple(string(), string(), 0);
}

bool RecordVariables::isDuplicate (string name) {

	for (unsigned it = 0; it < variables_to_write.size(); it++) {
		if (variables_to_write[it].second == name)
			return true;
	}

	return false;
}

bool RecordVariables::isDuplicatePointerVector (string name) {

	for (unsigned it = 0; it < pointer_vectors_to_write.size(); it++) {
		if (pointer_vectors_to_write[it].second == name)
			return true;
	}

	return false;
}

bool RecordVariables::isDuplicateVector (string name) {

	for (unsigned it = 0; it < vectors_to_write.size(); it++) {
		if (vectors_to_write[it].second == name)
			return true;
	}

	return false;
}

bool RecordVariables::isDuplicatePdf (string name) {

	for (unsigned pdf = 0; pdf < pdfs_to_write.size(); pdf++) {
		if (pdfs_to_write[pdf] == name)
			return true;
	}

	return false;
}

void RecordVariables::finalCheck (void) {
	size_t pos;
	unsigned position = 0;
	vector<unsigned> positions;

	for (unsigned var = 0; var < variables_to_write.size(); var++) {
		// check if there are still arrays
		if ((pos = variables_to_write[var].second.find("[")) != string::npos) {
			char array_pos = variables_to_write[var].second[pos + 1];

			variables_to_write[var].second.erase(variables_to_write[var].second.begin() + pos, variables_to_write[var].second.end());
			variables_to_write[var].second += "_";
			variables_to_write[var].second += array_pos;
		}

		bool first = true;
		// check if there are duplicates
		for (unsigned var2 = 0; var2 < variables_to_write.size(); var2++) {
			size_t pos = variables_to_write[var2].second.size() - 2;
			size_t pos_original = variables_to_write[var].second.size() - 2;


			if (!(isdigit(variables_to_write[var].second[variables_to_write[var].second.size()-1]) && (variables_to_write[var].second.at(pos_original) == '_'))) {
				if (isdigit(variables_to_write[var2].second[variables_to_write[var2].second.size()-1]) && (variables_to_write[var2].second.at(pos) == '_') && first) {
					if (variables_to_write[var2].second.find(variables_to_write[var].second) != string::npos) {
						positions.push_back(position);
						first = false;
					}
				}
			}
		}

		bool flag = true;
		for (unsigned i = position + 1; i < variables_to_write.size() && flag; i++) {
			if (variables_to_write[var].second == variables_to_write[i].second) {
				positions.push_back(position);
				flag = false;
			}
		}

		position++;
	}

	for (unsigned i = 0; i < positions.size(); i++)
		variables_to_write.erase(variables_to_write.begin() + positions[i]);
}

bool RecordVariables::checkArray (string var) {
	bool valid = false;
	size_t pos;

	if ((pos = var.find('[')) != string::npos) {
		var = var.substr(0, pos);
		string sc;

		// writes the array of an expression with specific position
		if (var.at(0) == '-')
			sc = var.substr(1);
		else
			sc = var;

		tuple<string, string, unsigned> arr = findArray(sc);

		// check if the size is 0 -> not found
		if (get<2>(arr))
			valid = true;
	} else {
		// writes array without specific position
		string sc;

		if (var.at(0) == '-')
			sc = var.substr(1);
		else
			sc = var;

		tuple<string, string, unsigned> arr = findArray(sc);

		if (get<2>(arr)) {
			valid = true;

			for (unsigned i = 0; i < get<2>(arr); ++i) {
				stringstream ss;

				ss << sc << "_" << i;
				variables_to_write.push_back(make_pair(get<0>(arr), ss.str()));
			}
		}
	}

	return valid;
}

string RecordVariables::remove_char (string s, char c) {
	stringstream ss;

	for (unsigned i = 0; i < s.size(); i++)
		if (s[i] != c)
			ss << s[i];

	return ss.str();
}

string RecordVariables::remove_space (string s) {
	stringstream ss;

	for (unsigned i = 0; i < s.size(); i++)
		if (s[i] !='\r' && s[i] !='\t' && s[i] != ' ' && s[i] != '\n')
			ss << s[i];

	return ss.str();
}

string RecordVariables::getExpressionName (string expr) {
	string res = boost::replace_all_copy(expr, "*","_t_");
	boost::replace_all(res, "+","_p_");
	boost::replace_all(res, "-","_m_");
	boost::replace_all(res, "/","_d_");
	boost::replace_all(res, "%","_i_");

	boost::replace_all(res, "[","_");
	//res.erase(std::remove_if(res.begin(), res.end(), [](char c){ return (c ==']');}), res.end());
	res = remove_char(res, ']');

	//remove spaces
	//res.erase(std::remove_if( res.begin(), res.end(), [](char c){ return (c =='\r' || c =='\t' || c == ' ' || c == '\n');}), res.end() );
	res = remove_space(res);

	return res;
}

string RecordVariables::replaceExpression (string expr) {
	stringstream new_string;

	expr.insert(0, "events[this_event_counter].");

	for (unsigned i = 0; i < expr.size(); i ++) {
		if (expr[i] == ' ' && expr[i+1] != '*' && expr[i+1] != '+' && expr[i+1] != '-' && expr[i+1] != '/' && expr[i+1] != '%') {
			new_string << " events[this_event_counter].";
			continue;
		}

		new_string << expr[i];
	}
	return new_string.str();
}

string RecordVariables::replaceMethod (string a) {
	stringstream as;

	for (unsigned i = 0; i < a.size(); i++)
		if (a[i] != '(' && a[i] != ')') {
			if (a[i] != '.')
				as << a[i];
			else
				as << '_';
		}

	return as.str();
}

string RecordVariables::getMethod (string a) {
	stringstream as;
	bool store = false;

	for (unsigned i = 0; i < a.size(); i++) {
		if (store)
			as << a[i];

		if (a[i] == '.') {
			store = true;

			as << a[i];
		}
	}

	return as.str();
}

void RecordVariables::writeVariables (string out_file, unsigned cuts) {
	string hd = filename;
	hd.erase(hd.end() - 8, hd.end());
	ifstream input_header (hd + ".h");
	ifstream input_body (filename);
	ofstream output_header (out_file + "_temp.h");
	ofstream output_body (out_file + "_temp.cxx");
	string line;
	stringstream branch_vars;
	bool writing_vars = false;
	bool in_rec = false;
	bool finalized = false;

	finalCheck();


	// Parse the header file
	while (getline(input_header, line)) {
		// check to start writing the variables, otherwise just write w/ever
		if (line.find(START_WRITING_HEADER) != string::npos) {
			writing_vars = true;
			output_header << line << endl;


			// new stuff 24 july 2018 - effort to remove ROOT from DataAnalysis
			output_header << "\tTTree *to_write;" << endl;
			if (variables_to_write.size() != 0 || expressions_to_write.size() != 0 || vectors_to_write.size() != 0 || pointer_vectors_to_write.size() != 0)
				output_header << "\tTTree **tree_rec_vars;" << endl;


			// write variables to save
			for (unsigned var = 0; var < variables_to_write.size(); var++) {

				string a = replaceMethod(variables_to_write[var].second);

				if (variables_to_write[var].second.find("()") != string::npos)
					output_header << "\tvector< double > **record_" << a << ";" << endl;
				else
					output_header << "\tvector< " << variables_to_write[var].first << " > **record_" << a << ";" << endl;
			}

			// write pointer vectors
			for (unsigned var = 0; var < pointer_vectors_to_write.size(); var++) {
				output_header << "\tvector< " << pointer_vectors_to_write[var].first << " > **record_" << pointer_vectors_to_write[var].second << ";" << endl;
			}

			// write vectors
			for (unsigned var = 0; var < vectors_to_write.size(); var++) {
				output_header << "\tvector< " << vectors_to_write[var].first << " > **record_" << vectors_to_write[var].second << ";" << endl;
			}

			// expressions
			for (unsigned expr = 0; expr < expressions_to_write.size(); expr++) {
				vector<string> types;
				string type;
				unsigned priority = 0;

				for (unsigned var= 0; var < variables_to_write.size(); var++)
					if (expressions_to_write[expr].find(variables_to_write[var].second) != string::npos)
						types.push_back(variables_to_write[var].first);


				for (unsigned u = 0; u < types.size(); u++)
					if((types[u].find("Double_t") != string::npos) || (types[u].find("double") != string::npos)) {
						if (priority < 4) {
							type = types[u];
							priority = 4;
						}
					}
					else
					if((types[u].find("Float_t") != string::npos) || (types[u].find("float") != string::npos)) {
						if (priority < 3) {
							type = types[u];
							priority = 3;
						}
					}
					else
					if((types[u].find("Int_t") != string::npos) || (types[u].find("int") != string::npos)) {
						if (priority < 2) {
							type = types[u];
							priority = 2;
						}
					}
					else
					if((types[u].find("UInt_t") != string::npos) || (types[u].find("unsigned") != string::npos)) {
						if (priority < 1) {
							type = types[u];
							priority = 1;
						}
					}

				expressions_type.push_back(type);
				output_header << "\tvector<" << type << "> **record_" << getExpressionName (expressions_to_write[expr]) << ";" << endl;
			}

		} else {
			
			if (line.find(STOP_WRITING_HEADER) != string::npos) {
				output_header << line << endl;
				writing_vars = false;
			} else {
				if (!writing_vars)
					output_header << line << endl;
			}
		}
	}

	writing_vars = false;

	// Parse the body file for the variables
	while (getline(input_body, line)) {
		if (line.find(START_WRITING_BODY) != string::npos) {
			in_rec = true;

			if (variables_to_write.size() != 0 || expressions_to_write.size() != 0 || vectors_to_write.size() != 0 || pointer_vectors_to_write.size() != 0) {
				writing_vars = true;
				output_body << line << endl;
				string last;


				output_body << "void " << out_file << "::initRecord (unsigned cuts) {" << endl;

				// new stuff 24 july 2018 - effort to remove ROOT from DataAnalysis
				output_body << "\ttree_rec_vars = new TTree*[number_of_cuts];" << endl << endl;
				output_body << "\tfor (unsigned i = 0; i < number_of_cuts; i++){" << endl;
				output_body << "\t\tstringstream s1, s2;\n\t\ts1 << \"Recorded_Vars_\";\n\t\ts1 << i;\n\t\ts2 << \"Variables recorded for cut \";\n\t\ts2 << i;"<< endl;
				output_body << "\t\ttree_rec_vars[i] = new TTree (s1.str().c_str(), s2.str().c_str());\n\t}" << endl << endl;

				// write variables to save
				for (unsigned var = 0; var < variables_to_write.size(); var++) {
					string to_write;
					// check if it's an array
					if (isdigit(variables_to_write[var].second[variables_to_write[var].second.size()-1]) && variables_to_write[var].second.at(variables_to_write[var].second.size() - 2) == '_') {
						to_write = variables_to_write[var].second.substr(0, variables_to_write[var].second.size() - 2);
					//	to_write += '[';
						to_write += '_';
						to_write += variables_to_write[var].second.at(variables_to_write[var].second.size() - 1);
					//	to_write += ']';
					} else {
						to_write = variables_to_write[var].second;
					}

					string a = replaceMethod(to_write);


					if (variables_to_write[var].second.find("()") != string::npos)
						output_body << "\trecord_" << a << " = new vector< double >*[cuts];" << endl;
					else
						output_body << "\trecord_" << a << " = new vector< " << variables_to_write[var].first << " >*[cuts];" << endl;
				}

				// write pointer vectors
				for (unsigned var = 0; var < pointer_vectors_to_write.size(); var++) {
					output_body << "\trecord_" << pointer_vectors_to_write[var].second << " = new vector< " << pointer_vectors_to_write[var].first << " >*[cuts];" << endl;
				}

				// write vectors
				for (unsigned var = 0; var < vectors_to_write.size(); var++) {
					output_body << "\trecord_" << vectors_to_write[var].second << " = new vector< " << vectors_to_write[var].first << " >*[cuts];" << endl;
				}

				// expressions
				for (unsigned expr = 0; expr < expressions_to_write.size(); expr++) {
					output_body << "\trecord_" << getExpressionName (expressions_to_write[expr]) << " = new vector< " << expressions_type[expr] << ">*[cuts];" << endl;
				}

				output_body << endl;

				output_body << "\trecord_vars = new boost::mutex[cuts];" << endl << endl;

				// fill the array
				output_body << "\tfor (unsigned i = 0; i < cuts; i++) {" << endl;
				for (unsigned var = 0; var < variables_to_write.size(); var++) {
					string to_write;
					// check if it's an array
					if (isdigit(variables_to_write[var].second[variables_to_write[var].second.size()-1]) && variables_to_write[var].second.at(variables_to_write[var].second.size() - 2) == '_') {
						to_write = variables_to_write[var].second.substr(0, variables_to_write[var].second.size() - 2);
					//	to_write += '[';
						to_write += '_';
						to_write += variables_to_write[var].second.at(variables_to_write[var].second.size() - 1);
					//	to_write += ']';
					} else {
						to_write = variables_to_write[var].second;
					}

					string a = replaceMethod(to_write);


					if (variables_to_write[var].second.find("()") != string::npos)
						output_body << "\t\trecord_" << a << "[i] = new vector< double > (0);" << endl;
					else
						output_body << "\t\trecord_" << a << "[i] = new vector< " << variables_to_write[var].first << " > (0);" << endl;
				}

				// write pointer vectors
				for (unsigned var = 0; var < pointer_vectors_to_write.size(); var++) {
					output_body << "\t\trecord_" << pointer_vectors_to_write[var].second << "[i] = new vector< " << pointer_vectors_to_write[var].first << " > (0);" << endl;
				}

				// write vectors
				for (unsigned var = 0; var < vectors_to_write.size(); var++) {
					output_body << "\t\trecord_" << vectors_to_write[var].second << "[i] = new vector< " << vectors_to_write[var].first << " > (0);" << endl;
				}

				for (unsigned expr = 0; expr < expressions_to_write.size(); expr++) {
					output_body << "\t\trecord_" << getExpressionName (expressions_to_write[expr]) << "[i] = new vector< " << expressions_type[expr] << "> (0);" << endl;
				}
				output_body << "\t}" << endl << endl;

				output_body << "}" << endl << endl;
				//	output_body << "#endif" << endl << endl;

				output_body << "void " << out_file << "::recordVariables (unsigned cut_number, long unsigned this_event_counter) {" << endl;

				output_body << "\trecord_vars[cut_number].lock();" << endl << endl<< endl;

				// write variables to save
				for (unsigned var = 0; var < variables_to_write.size(); var++) {
					string to_write1, to_write2;
					// check if it's an array
					if (isdigit(variables_to_write[var].second[variables_to_write[var].second.size()-1]) && variables_to_write[var].second.at(variables_to_write[var].second.size() - 2) == '_') {
						to_write2 = variables_to_write[var].second.substr(0, variables_to_write[var].second.size() - 2);
						to_write2 += '[';
						//to_write += '_';
						to_write2 += variables_to_write[var].second.at(variables_to_write[var].second.size() - 1);
						to_write2 += ']';

						to_write1 = variables_to_write[var].second.substr(0, variables_to_write[var].second.size() - 2);
						to_write1 += '_';
						to_write1 += variables_to_write[var].second.at(variables_to_write[var].second.size() - 1);
					} else {
						to_write1 = variables_to_write[var].second;
						to_write2 = variables_to_write[var].second;
					}

					string a = replaceMethod(to_write1);
					string b = replaceMethod(to_write2);

					last = a;

					// check if it's a vector (with pointer)
					if (variables_to_write[var].first.find("vector") != string::npos) {
						output_body << "\n\t" << variables_to_write[var].first << " *aux_" << a << " = events[this_event_counter]." << to_write2 << ";" << endl;
						output_body << "\trecord_" << variables_to_write[var].second << "[cut_number]->push_back(*aux_" << a << ");" << endl;
					} else {
						output_body << "\trecord_" << a << "[cut_number]->push_back(events[this_event_counter]." << to_write2 << ");" << endl;
					}
				}

				// write pointer vectors
				for (unsigned var = 0; var < pointer_vectors_to_write.size(); var++) {
					unsigned v2;
					stringstream as;
					for (v2 = pointer_vectors_to_write[var].second.size() - 1; pointer_vectors_to_write[var].second[v2] !='_'; v2--)
						as <<pointer_vectors_to_write[var].second[v2];

					string ax = pointer_vectors_to_write[var].second.substr(0, v2);
					output_body << "\tif (events[this_event_counter]." << ax << "->size() > " << as.str() <<") ";
					output_body << "record_" << pointer_vectors_to_write[var].second << "[cut_number]->push_back(events[this_event_counter]." << ax << "->at(" << as.str() << "));" << endl;
				}

				// write vectors
				for (unsigned var = 0; var < vectors_to_write.size(); var++) {
					unsigned v2;
					stringstream as;
					for (v2 = vectors_to_write[var].second.size() - 1; vectors_to_write[var].second[v2] !='_'; v2--)
						as <<vectors_to_write[var].second[v2];

					string ax = vectors_to_write[var].second.substr(0, v2);
					output_body << "\tif (events[this_event_counter]." << ax << ".size() > " << as.str() <<") ";
					output_body << "record_" << vectors_to_write[var].second << "[cut_number]->push_back(events[this_event_counter]." << ax << ".at(" << as.str() << "));" << endl;
				}

				output_body << endl;

				// write expressions
				for (unsigned expr = 0; expr < expressions_to_write.size(); expr++) {
					string aux_exp = replaceExpression(expressions_to_write[expr]);
					output_body << "\trecord_" << getExpressionName (expressions_to_write[expr]) << "[cut_number]->push_back(" << aux_exp << ");" << endl;
				}

				output_body << endl << "\trecord_vars[cut_number].unlock();" << endl;

				output_body << "}" << endl << endl;


				output_body << "void " << out_file << "::recordVariablesBoost (long unsigned this_event_counter) {" << endl << endl;

				// write variables to save
				for (unsigned var = 0; var < variables_to_write.size(); var++) {
					string to_write1, to_write2;
					// check if it's an array
					if (isdigit(variables_to_write[var].second[variables_to_write[var].second.size()-1]) && variables_to_write[var].second.at(variables_to_write[var].second.size() - 2) == '_') {
						to_write2 = variables_to_write[var].second.substr(0, variables_to_write[var].second.size() - 2);
						to_write2 += '[';
						//to_write += '_';
						to_write2 += variables_to_write[var].second.at(variables_to_write[var].second.size() - 1);
						to_write2 += ']';

						to_write1 = variables_to_write[var].second.substr(0, variables_to_write[var].second.size() - 2);
						to_write1 += '_';
						to_write1 += variables_to_write[var].second.at(variables_to_write[var].second.size() - 1);
					} else {
						to_write1 = variables_to_write[var].second;
						to_write2 = variables_to_write[var].second;
					}

					string a = replaceMethod(to_write1);
					string b = replaceMethod(to_write2);

					last = a;

					// check if it's a vector (with pointer)
					if (variables_to_write[var].first.find("vector") != string::npos) {
						output_body << "\n\t" << variables_to_write[var].first << " *aux_" << a << " = events[this_event_counter]." << to_write2 << ";" << endl;
						output_body << "\trecord_" << to_write1 << "[0]->push_back(*aux_" << to_write2 << ");" << endl;
					} else {
						output_body << "\trecord_" << a << "[0]->push_back(events[this_event_counter]." << to_write2 << ");" << endl;
					}
				}

				// write pointer vectors
				for (unsigned var = 0; var < pointer_vectors_to_write.size(); var++) {
					unsigned v2;
					stringstream as;
					for (v2 = pointer_vectors_to_write[var].second.size() - 1; pointer_vectors_to_write[var].second[v2] !='_'; v2--)
						as <<pointer_vectors_to_write[var].second[v2];

					string ax = pointer_vectors_to_write[var].second.substr(0, v2);
					output_body << "\tif (events[this_event_counter]." << ax << "->size() > " << as.str() <<") ";
					output_body << "record_" << pointer_vectors_to_write[var].second << "[0]->push_back(events[this_event_counter]." << ax << "->at(" << as.str() << "));" << endl;
				}

				// write vectors
				for (unsigned var = 0; var < vectors_to_write.size(); var++) {
					unsigned v2;
					stringstream as;
					for (v2 = vectors_to_write[var].second.size() - 1; vectors_to_write[var].second[v2] !='_'; v2--)
						as <<vectors_to_write[var].second[v2];

					string ax = vectors_to_write[var].second.substr(0, v2);
					output_body << "\tif (events[event]." << ax << ".size() > " << as.str() <<") ";
					output_body << "record_" << vectors_to_write[var].second << "[0]->push_back(events[this_event_counter]." << ax << ".at(" << as.str() << "));" << endl;
				}

				output_body << endl;

				// write expressions
				for (unsigned expr = 0; expr < expressions_to_write.size(); expr++) {
					string aux_exp = replaceExpression(expressions_to_write[expr]);
					output_body << "\trecord_" << getExpressionName (expressions_to_write[expr]) << "[0]->push_back(" << aux_exp << ");" << endl;
				}

				output_body << "}" << endl << endl;


				// Write the vars to the output ttree branches
				output_body << "void " << out_file << "::writeVariables (void) {" << endl;
				output_body << "\tTFile *tree_file = new TFile (tree_filename.c_str(), \"recreate\");" << endl << endl;
				output_body << "\ttree_file->cd();" << endl << endl;
				output_body << "\tfor (unsigned i = 0; i < number_of_cuts; i++) {" << endl;

				if (!last.empty())
					output_body << "\t\tif (!record_" << last << "[i]->empty()) {" << endl;
				else
					output_body << "\t\t{"<< endl;

				// vars and expressions
				for (unsigned var = 0; var < variables_to_write.size(); var++) {
					string to_write;
					// check if it's an array
					if (isdigit(variables_to_write[var].second[variables_to_write[var].second.size()-1]) && variables_to_write[var].second.at(variables_to_write[var].second.size() - 2) == '_') {
						to_write = variables_to_write[var].second.substr(0, variables_to_write[var].second.size() - 2);
						// to_write += '[';
						to_write += '_';
						to_write += variables_to_write[var].second.at(variables_to_write[var].second.size() - 1);
						// to_write += ']';
					} else {
						to_write = variables_to_write[var].second;
					}

					string b = replaceMethod(to_write);

					if (to_write.find("(") != string::npos)
						output_body << "\t\t\tdouble aux_" << b << ";" << endl;
					else
						output_body << "\t\t\t" << variables_to_write[var].first << " aux_" << b << ";" << endl;

					// check if it's a vector
					if (variables_to_write[var].first.find("vector") != string::npos) {
						output_body << "\t\t\ttree_rec_vars[i]->Branch(\"" << to_write << "\", &record_" << to_write << "[i].at(0).at(0));" << endl;
					} else {
						output_body << "\t\t\ttree_rec_vars[i]->Branch(\"" << to_write << "\", &aux_" << b << ");" << endl;

						// branch_vars << "\t\t\t\t\trecord_" << b << "[i]->pop_back(aux_" << b << ");" << endl;
						branch_vars << "\t\t\t\t\taux_" << b << " = record_" << b << "[i]->back();" << endl;
						branch_vars << "\t\t\t\t\trecord_" << b << "[i]->pop_back();" << endl;
					}
				}

				// write pointer vectors
				for (unsigned var = 0; var < pointer_vectors_to_write.size(); var++) {
					output_body << "\t\t\t" << pointer_vectors_to_write[var].first << " aux_" << pointer_vectors_to_write[var].second << ";" << endl;
					output_body << "\t\t\ttree_rec_vars[i]->Branch(\"aux_" << pointer_vectors_to_write[var].second << "\", &aux_" << pointer_vectors_to_write[var].second << ");" << endl;
				}

				// write vectors
				for (unsigned var = 0; var < vectors_to_write.size(); var++) {
					output_body << "\t\t\t" << vectors_to_write[var].first << " aux_" << vectors_to_write[var].second << ";" << endl;
					output_body << "\t\t\ttree_rec_vars[i]->Branch(\"aux_" << vectors_to_write[var].second << "\", &aux_" << vectors_to_write[var].second << ");" << endl;
				}

				unsigned exp_counter = 0;
				for (unsigned expr = 0; expr < expressions_to_write.size(); expr++) {
					last = getExpressionName (expressions_to_write[expr]);
					output_body << "\t\t\t"<< expressions_type[exp_counter] << " aux_" << getExpressionName (expressions_to_write[expr]) << ";" << endl;
					output_body << "\t\t\ttree_rec_vars[i]->Branch(\"" << getExpressionName (expressions_to_write[expr]) << "\", &aux_" << getExpressionName (expressions_to_write[expr]) << ");" << endl;

					// branch_vars << "\t\t\t\t\trecord_" << getExpressionName (expressions_to_write[expr]) << "[i]->pop_back(aux_" << getExpressionName (expressions_to_write[expr]) << ");" << endl;
					branch_vars << "\t\t\t\t\taux_" << getExpressionName (expressions_to_write[expr]) << " = record_" << getExpressionName (expressions_to_write[expr]) << "[i]->back();" << endl;
					branch_vars << "\t\t\t\t\trecord_" << getExpressionName (expressions_to_write[expr]) << "[i]->pop_back();" << endl;

					exp_counter++;
				}

				// ERRO????

				if (!last.empty()) {
					output_body << "\n\t\t\twhile (!record_" << last << "[i]->empty()) {" << endl;
				} else
					output_body << "\n\t\t\tfor (unsigned j = 0; j < number_events; j++) {" << endl;

				output_body << branch_vars.str();


				// write pointer vectors
				for (unsigned var = 0; var < pointer_vectors_to_write.size(); var++) {
					output_body << "\t\t\t\t\tif (!record_" << pointer_vectors_to_write[var].second << "[i]->empty()) {" << endl;
					// output_body << "record_" << pointer_vectors_to_write[var].second << "[i]->pop_back(aux_" << pointer_vectors_to_write[var].second << ");" << endl;
					output_body << "\t\t\t\t\t\taux_" << pointer_vectors_to_write[var].second << " = record_" << pointer_vectors_to_write[var].second << "[i]->back();" << endl;
					output_body << "\t\t\t\t\t\trecord_" << pointer_vectors_to_write[var].second << "[i]->pop_back();\n\t\t\t\t\t}" << endl;
				}

				// write vectors
				for (unsigned var = 0; var < vectors_to_write.size(); var++) {
					output_body << "\t\t\t\t\tif (!record_" << vectors_to_write[var].second << "[i]->sempty()) {" << endl;
					// output_body << "record_" << vectors_to_write[var].second << "[i]->pop_back(aux_" << vectors_to_write[var].second << ");" << endl;
					output_body << "\t\t\t\t\t\taux_" << vectors_to_write[var].second << " = record_" << vectors_to_write[var].second << "[i]->back();" << endl;
					output_body << "\t\t\t\t\t\trecord_" << vectors_to_write[var].second << "[i]->pop_back();\n\t\t\t\t\t}" << endl;
				}

				output_body << "\t\t\t\t\ttree_rec_vars[i]->Fill();\n\t\t\t\t}" << endl << endl;
				output_body << "\t\t\ttree_rec_vars[i]->Write();" << endl;
				output_body << "\t\t}\n\t}\n\tdelete tree_file;\n}" << endl << endl;
			} else {
				output_body << line << endl;
			}
		} else {
			//cout << endl << endl << in_rec << "\t" << !writing_vars << "\t" << !finalized << endl << endl;
			if (in_rec && !writing_vars && !finalized) {
				output_body << "void " << out_file << "::initRecord (unsigned cuts) {" << endl << endl;
				output_body << "}" << endl << endl;

				output_body << "void " << out_file << "::recordVariables (unsigned cut_number, long unsigned this_event_counter) {" << endl << endl;
				output_body << "}" << endl << endl;

				output_body << "void " << out_file << "::recordVariablesBoost (long unsigned event) {" << endl << endl;
				output_body << "}" << endl << endl;

				output_body << "void " << out_file << "::writeVariables (void) {" << endl << endl;
				output_body << "}" << endl << endl;

				finalized = true;
			}

			if (line.find(STOP_WRITING_BODY) != string::npos) {
				writing_vars = false;
				in_rec = false;
			}

			if (!in_rec && !writing_vars) {
				

				output_body << line << endl;
			}
		}
	}

	input_body.close();
	input_header.close();
	input_body.open(filename);
	input_header.open(hd + ".h");
}

void RecordVariables::writePdfs (string out_file) {
	ifstream input_header (out_file + "_temp.h");
	ifstream input_body (out_file + "_temp.cxx");
	ofstream output_header (out_file + "_final.h");
	ofstream output_body (out_file + "_final.cxx");
	string line;

	bool writing_vars = false;
	bool in_rec = false;
	bool after_rec = false;


	// Parse the header file
	while (getline(input_header, line)) {
		// check to start writing the variables, otherwise just write w/ever
		if (line.find(START_WRITING_PDF_HEADER) != string::npos) {
			writing_vars = true;
			output_header << line << endl;
			// write pdfs to save
			for (unsigned pdf = 0; pdf < pdfs_to_write.size(); pdf++) {
				string a = replaceMethod(pdfs_to_write[pdf]);

				output_header << "\tvector< double > pdf_" << a << ";" << endl;
			}
		} else {
			if (line.find(STOP_WRITING_PDF_HEADER) != string::npos) {
				output_header << line << endl;
				writing_vars = false;
			} else
				if (!writing_vars)
					output_header << line << endl;
		}
	}

	// Parse the body file for the pdfs
	while (getline(input_body, line)) {
		if (line.find(START_WRITING_PDF_BODY) != string::npos) {
			output_body << line << endl;
			in_rec = true;
			writing_vars = true;

			if (pdfs_to_write.size() != 0) {
				output_body << "void " << out_file << "::recordPdfs (void) {" << endl;

				for (unsigned pdf = 0; pdf < pdfs_to_write.size(); pdf++) {
					string to_write;
					// check if it's an array
					if (isdigit(pdfs_to_write[pdf][pdfs_to_write[pdf].size()-1]) && pdfs_to_write[pdf].at(pdfs_to_write[pdf].size() - 2) == '_') {
						to_write = pdfs_to_write[pdf].substr(0, pdfs_to_write[pdf].size() - 2);
						// to_write += '[';
						to_write += '_';
						to_write += pdfs_to_write[pdf].at(pdfs_to_write[pdf].size() - 1);
						// to_write += ']';
					} else {
						to_write = pdfs_to_write[pdf];
					}

					string b = replaceMethod(to_write);

					output_body << "\tpdf_" << b << ".push_back(events[this_event_counter]." << to_write << ");" << endl;
				}

				output_body << "}" << endl << endl;



				output_body << "void " << out_file << "::writePdfs (void) {" << endl;
				output_body << "\tTFile f (\"pdf_file.root\", \"recreate\");" << endl << endl;

				for (unsigned pdf = 0; pdf < pdfs_to_write.size(); pdf++) {
					string to_write;
					// check if it's an array
					if (isdigit(pdfs_to_write[pdf][pdfs_to_write[pdf].size()-1]) && pdfs_to_write[pdf].at(pdfs_to_write[pdf].size() - 2) == '_') {
						to_write = pdfs_to_write[pdf].substr(0, pdfs_to_write[pdf].size() - 2);
						// to_write += '[';
						to_write += '_';
						to_write += pdfs_to_write[pdf].at(pdfs_to_write[pdf].size() - 1);
						// to_write += ']';
					} else {
						to_write = pdfs_to_write[pdf];
					}

					string b = replaceMethod(to_write);

					output_body << "\tTH1D hist_pdf_" << b << " (\"hist_pdf_" << b << "\", \"Histogram of the " << to_write <<
					" pdf\", pdf_"<<pdfs_to_write[pdf]<<".size(), *min_element(pdf_" << pdfs_to_write[pdf] << ".begin(), pdf_"<<pdfs_to_write[pdf]<<".end()), *max_element(pdf_" << pdfs_to_write[pdf] << ".begin(), pdf_"<<pdfs_to_write[pdf]<<".end()));" << endl;
				}


				output_body << endl << "\tfor (unsigned i = 0; i < pdf_" << pdfs_to_write[0] << ".size(); i++) {" << endl;


				for (unsigned pdf = 0; pdf < pdfs_to_write.size(); pdf++) {
					string to_write;
					// check if it's an array
					if (isdigit(pdfs_to_write[pdf][pdfs_to_write[pdf].size()-1]) && pdfs_to_write[pdf].at(pdfs_to_write[pdf].size() - 2) == '_') {
						to_write = pdfs_to_write[pdf].substr(0, pdfs_to_write[pdf].size() - 2);
						// to_write += '[';
						to_write += '_';
						to_write += pdfs_to_write[pdf].at(pdfs_to_write[pdf].size() - 1);
						// to_write += ']';
					} else {
						to_write = pdfs_to_write[pdf];
					}

					string b = replaceMethod(to_write);

					output_body << "\t\thist_pdf_" << b << ".Fill(pdf_" << b << "[i]);" << endl;
				}

				output_body << "\t}" << endl << endl;
				output_body << "\tf.Write();" << endl;
				output_body << "}" << endl << endl;
			} else {
				output_body << "void " << out_file << "::recordPdfs (void) {" << endl;
				output_body << endl << "}" << endl << endl;

				output_body << "void " << out_file << "::writePdfs (void) {" << endl;
				output_body << endl << "}" << endl << endl;
			}
		} else {
			if (line.find(STOP_WRITING_PDF_BODY) != string::npos)
				after_rec = true;
			if (!in_rec || after_rec) {
				output_body << line << endl;
			}
		}
	}

	input_body.close();
	input_header.close();
	output_body.close();
	output_header.close();
}

string RecordVariables::getType (string name) {

	// check on arrays
	for (unsigned it = 0; it < arrays.size(); ++it) {
		string aux = get<1>(arrays[it]);
		if (aux[0] == '*')
			aux.erase(0,1);

		if (aux == name)
			return get<0>(arrays[it]);
	}

	// check on scalars
	for (vector< pair<string, string> >::iterator it = scalars.begin(); it != scalars.end(); ++it) {
		string aux = it->second;
		if (aux[0] == '*')
			aux.erase(0,1);

		if (aux == name)
			return it->first;
	}

	return string();
}

void RecordVariables::parsePdfs (void) {
	ifstream file (filename);
	string line;
	bool parse = false;

	// loops over the file
	while (getline(file, line)) {
		// check to start or stop parsing
		if (line.find(START_PARSING_PDF_FILE) != string::npos) {
			parse = true;
			continue;
		} else
			if (parse && line.find(STOP_PARSING_VARIABLES_FILE) != string::npos)
				parse = false;

		if (parse) {
			stringstream strstr (line);

			istream_iterator<string> it(strstr);
			istream_iterator<string> end;
			vector<string> results(it, end);

			if (results.empty())
				continue;

			// check if the line has a single scalar
			if (results.size() == 1) {

				string a = results[0];

				size_t a_pos = a.find(".");

				if (a_pos != string::npos)
					a.erase(a_pos, a.size() - 1);

				if (checkVariable(a)) {
					// avoid duplicates
					if (!isDuplicatePdf(a)) {
						pdfs_to_write.push_back(results[0]);
					}
				}
			} else {
				// check for the expression operand
				bool valid [results.size() / 2];

				for (unsigned i = 1; i < results.size(); i += 2) {
					valid[i / 2] = false;

					for (unsigned op = 0; op < operands.size(); op++)
						if (results[i] == operands[op])
							valid[i / 2] = true;
				}

				if (results.size() % 2 != 1)
					valid[0] = false;

				// checks if all operands are valid
				for (unsigned i = 0; i < results.size() / 2; ++i)
					valid[0] = valid[0] && valid[i];

				if (valid[0]) {
					bool vars_exist = true;
					// avoid duplicates of the variables
					for (unsigned i = 0; i < results.size() && vars_exist; i += 2){
						if ( (vars_exist = checkVariable(results[i])) ){
							if (!isDuplicate(results[i])) {
								size_t pos;
								string aux;

								if((pos = results[i].find("[")) != string::npos){
									aux = results[i].substr(0, pos);
									boost::replace_all(results[i], "[","_");
									results[i] = remove_char(results[i], ']');
								} else
									aux = results[i];
							}
						}
					}

					// add the expression
					if (vars_exist)
						pdfs_to_write.push_back(line);
				} else {
					cerr << "Invalid expression: " << line << endl;
				}
			}
		}
	}

}

void RecordVariables::parseVariables (string out_file, unsigned cuts) {
	ifstream file (filename);
	string line;
	bool parse = false;

	// loops over the file
	while (getline(file, line)) {
		// check to start or stop parsing
		if (line.find(START_PARSING_VARIABLES_FILE) != string::npos) {
			parse = true;
			continue;
		} else
			if (parse && line.find(STOP_PARSING_VARIABLES_FILE) != string::npos)
				parse = false;

		if (parse) {
			stringstream strstr (line);

			istream_iterator<string> it(strstr);
			istream_iterator<string> end;
			vector<string> results(it, end);

			if (results.empty())
				continue;

			// check if the line has a single scalar (no expression)
			if (results.size() == 1) {

				string a = results[0];

				size_t a_pos = a.find(".");

				if (a_pos != string::npos)
					a.erase(a_pos, a.size() - 1);

				if(!checkPointerVector(a))
					if(!checkVector(a))
						if (checkVariable(a)) {
							// avoid duplicates
							if (!isDuplicate(a)) {
								variables_to_write.push_back(make_pair(getType(a), results[0]));
							}
						}
			} else {
				// check for the expression operand
				bool valid [results.size() / 2];

				for (unsigned i = 1; i < results.size(); i += 2) {
					valid[i / 2] = false;

					for (unsigned op = 0; op < operands.size(); op++)
						if (results[i] == operands[op])
							valid[i / 2] = true;
				}

				if (results.size() % 2 != 1)
					valid[0] = false;

				// checks if all operands are valid
				for (unsigned i = 0; i < results.size() / 2; ++i)
					valid[0] = valid[0] && valid[i];

				if (valid[0]) {
					bool vars_exist = true;
					// avoid duplicates of the variables
					for (unsigned i = 0; i < results.size() && vars_exist; i += 2){
						if ( (vars_exist = checkVariable(results[i])) ){
							if (!isDuplicate(results[i])) {
								size_t pos;
								string aux;

								if((pos = results[i].find("[")) != string::npos){
									aux = results[i].substr(0, pos);
									boost::replace_all(results[i], "[","_");
									results[i] = remove_char(results[i], ']');
								} else
									aux = results[i];

								// comparacao errada
								variables_to_write.push_back(make_pair(getType(aux), results[i]));
							}
						}
					}

					// add the expression
					if (vars_exist)
						expressions_to_write.push_back(line);
				} else {
					cerr << "Invalid expression: " << line << endl;
				}
			}
		}
	}

	file.close();

	writeVariables(out_file, cuts);

	//parsePdfs();
	writePdfs(out_file);
}

void RecordVariables::parseDelphes (string out_file, unsigned cuts) {

	// scalars
	scalars.push_back(make_pair("HEPParticle", "MET"));
	scalars.push_back(make_pair("HEPParticle", "MHT"));
	scalars.push_back(make_pair("double", "TET"));
	scalars.push_back(make_pair("double", "THT"));
	scalars.push_back(make_pair("unsigned", "nparts"));
	scalars.push_back(make_pair("unsigned", "processId"));
	scalars.push_back(make_pair("double", "weight"));
	scalars.push_back(make_pair("double", "scale"));
	scalars.push_back(make_pair("double", "alphaQED"));
	scalars.push_back(make_pair("double", "alphaQCD"));
	scalars.push_back(make_pair("double", "PDFscale"));

	// vectors (arrays)
	arrays.push_back(make_tuple("HEPPhoton", "photons", MAX_VECTOR_SIZE));
	arrays.push_back(make_tuple("HEPLepton", "electrons", MAX_VECTOR_SIZE));
	arrays.push_back(make_tuple("HEPLepton", "muons", MAX_VECTOR_SIZE));
	arrays.push_back(make_tuple("HEPTau", "taus", MAX_VECTOR_SIZE));
	arrays.push_back(make_tuple("HEPJet", "jets", MAX_VECTOR_SIZE));
	arrays.push_back(make_tuple("HEPTrack", "tracks", MAX_VECTOR_SIZE));
	arrays.push_back(make_tuple("HEPMCParticle", "particles", MAX_VECTOR_SIZE));

	parseVariables (out_file, cuts);
}

void RecordVariables::parse (string out_file, unsigned cuts, bool type) {

	if (!type) {
		parseEventFile();
	/*	cout << "-> Scalars" << endl;
		for (auto e : scalars)
			cout << e.first << "\t" << e.second << endl;
		cout << "-> Pointer Vectors" << endl;
		for (auto e : pointer_vectors)
			cout << e.first << "\t" << e.second << endl;
		cout <<endl<< "-> Arrays" << endl;
		for (unsigned e = 0; e < arrays.size(); e++)
			cout << get<0>(arrays[e]) << "\t" << get<1>(arrays[e]) << "\t" << get<2>(arrays[e]) << endl;
		return;*/

		// parse regular files
		parseVariables (out_file, cuts);
	} else {
		// parse delphes files
		parseDelphes (out_file, cuts);
	}
}
