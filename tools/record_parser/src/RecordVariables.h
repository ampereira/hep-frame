#ifndef RECORDVARIABLES_h
#define RECORDVARIABLES_h

#include <cstdlib>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <map>
#include <ctype.h>
#include <boost/algorithm/string.hpp>


#define START_PARSING_EVENT_FILE "public:"
#define STOP_PARSING_EVENT_FILE "HEPEvent (TTree *tree);"
#define START_PARSING_VARIABLES_FILE "#ifdef RecordVariables"
#define START_PARSING_PDF_FILE "#ifdef PdfVariables"
#define STOP_PARSING_VARIABLES_FILE "#endif"
#define DECLARE_VARIABLES "\t// Declaration of variables to record"
#define END_DECLARE_VARIABLES "\t// End of declaration of variables to record"
#define START_WRITING_HEADER "// Variables to record per cut"
#define START_WRITING_PDF_HEADER "// Pdfs to record"
//#define STOP_WRITING_HEADER "// Pdfs to record"
#define STOP_WRITING_HEADER "// Insert your class variables here"
#define STOP_WRITING_PDF_HEADER "// Insert your class variables here"
#define START_WRITING_BODY "// Method to record variables, do not edit"
//#define STOP_WRITING_BODY "// Write here the variables and expressions to record per cut"
#define STOP_WRITING_BODY "// end"
#define START_WRITING_PDF_BODY "// Method to record pdfs, do not edit"
#define STOP_WRITING_PDF_BODY "// Write here the variables to record as pdf"
#define MAX_VECTOR_SIZE 9999999

using namespace std;

class RecordVariables {
	string filename;
	string event_filename;
	string last;

	vector< pair<string, string> > scalars;	// scalar event variable list (type, name)
	vector< pair<string, string> > vectors;	// vector event variable list (type, name)
	vector< pair<string, string> > pointer_vectors;	// pointer to vector event variable list (type, name)
	vector< tuple<string, string, unsigned> > arrays;	// array event variable list ((type, name), size)
	vector<string> operands;	// operands * + - / %
	vector<string> functions;	// functions cos, sin, etc, later extended
	vector< pair<string, string> > variables_to_write;	// write (type, variable)
	vector< pair<string, string> > pointer_vectors_to_write;	// write (type, variable)
	vector< pair<string, string> > vectors_to_write;	// write (type, variable)
	vector<string> expressions_to_write;
	vector<string> expressions_type;
	vector<string> pdfs_to_write;

	vector<string> untokenize (vector<string> res);
	string remove_char (string s, char c);
	string remove_space (string s);
	void parseEventFile (void);
	bool checkVariable (string var);
	bool checkArray (string var);
	bool checkVector (string var);
	bool checkPointerVector (string var);
	void writeVariables (string out_file, unsigned cuts);
	void writePdfs (string out_file);
	tuple <string, string, unsigned> findArray (string name);
	string getType (string name);
	bool isDuplicate (string name);
	bool isDuplicateVector (string name);
	bool isDuplicatePointerVector (string name);
	bool isDuplicatePdf (string name);
	void finalCheck (void);
	string getExpressionName (string expr);
	string replaceExpression (string expr);
	string replaceMethod (string a);
	string getMethod (string a);
	void parsePdfs (void);
	void parseVariables (string out_file, unsigned cuts);
	void parseDelphes (string out_file, unsigned cuts);

public:
	RecordVariables (string _filename, string _event_filename);
	void parse (string out_file, unsigned cuts, bool type);
};

#endif
